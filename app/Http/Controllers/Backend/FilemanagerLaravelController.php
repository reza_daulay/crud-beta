<?php 

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Pqb\FilemanagerLaravel\FilemanagerLaravel;

use Image;
use File;

class FilemanagerLaravelController extends Controller {
	public function __construct(){
		// $this->middleware('auth');
	}

	public function postUpload(Request $request)
	{
		if ($request->hasFile('upload'))
		{
			if ( !in_array( strtolower($request->file('upload')->getClientOriginalExtension()) ,config('news.image.allow_extensions')) )
				return '<script>alert(\'File Gagal Diupload , Cek Ekstensi Gambar\')</script>';
			else if( $request->file('upload')->getSize() > config('news.image.uploader.size') )
				return '<script>alert(\'Ukuran gambar melebihi 150KB\')</script>';

			$img = Image::make($request->file('upload'));
			$path = !is_null($request->path) && $request->path !== '' && $request->path !== '/' ? $request->path : 'news';
			$filename = $request->file('upload')->getClientOriginalName();
			if (File::exists('assets/image/'.$path.'/uploader/'.date('Y').'/'.date('m').'/'.$filename)){
				$filename = date('dmYHis').'-'.$filename;
			}
			$request->file('upload')->move('assets/image/'.$path.'/uploader/'.date('Y').'/'.date('m'),$filename);
			return '<script>
				window.parent.CKEDITOR.tools.callFunction(
				'.$request->CKEditorFuncNum.', 
				\''.asset('assets/image/'.$path.'/uploader/'.date('Y').'/'.date('m').'/'.$filename).'\', 
				null
				)
			</script>';
		}
	}
	
	public function getShow()
	{
		return view('filemanager-laravel::filemanager.index');
	}

    public function getConnectors()
	{
		$extraConfig = array('dir_filemanager'=> '/assets/backend');
		$f = FilemanagerLaravel::Filemanager($extraConfig);
		$f->connector_url = url('/').'/assets/backend/filemanager/connectors';
		$f->setFileRoot('assets/image/news/uploader/');
		$f->run();
	}

	public function postConnectors()
	{
		$extraConfig = array('dir_filemanager'=> '/assets/backend');
		$f = FilemanagerLaravel::Filemanager($extraConfig);
		$f->connector_url = url('/').'/assets/backend/filemanager/connectors';
		$f->setFileRoot('assets/image/news/uploader/');
		$f->run();
	}

}
