<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\News;
use App\NewsImage;
use App\Newscategory;
use App\Tag;

use Validator;
use File;
use App\Helpers\FileHelpers;

use Auth;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		/* sort */
		$table_sort_by_field = trim($request->get('table_sort_by_field'));
		$table_sort_by_field = !empty($table_sort_by_field)? $table_sort_by_field : null;
		$table_sort_by_order = trim($request->get('table_sort_by_order'));
		$table_sort_by_order = !empty($table_sort_by_order)? $table_sort_by_order : null;

		/* search */
		$start_date = trim($request->get('start_date'));
		$start_date = !empty($start_date)? $start_date : null;
		$end_date = trim($request->get('end_date'));
		$end_date = !empty($end_date)? $end_date : null;
		$newscategory_id = trim($request->get('newscategory_id'));
		$newscategory_id = !empty($newscategory_id)? $newscategory_id : null;
		$title = trim($request->get('title'));
		$title = !empty($title)? $title : null;
		$created_by = trim($request->get('created_by'));
		$created_by = !empty($created_by)? $created_by : null;


        $news = News::with(['newscategory', 'imageCover', 'writer']);
		if(!is_null($start_date) && !empty($end_date)){
			$news = $news->whereBetween('posted_at', [$start_date.' 00:00:00',$end_date.' 23:59:59']);
		}
		else{
            $news = $news->whereRaw('posted_at >= DATE_SUB(curdate(), INTERVAL 1 WEEK)');
			//$news->whereBetween('posted_at', ['2016-09-25 00:01:00', date('Y-m-d H:i:s')]);
		}
		if (!is_null($newscategory_id)){
			$news = $news->where('newscategory_id', $newscategory_id);
		}
        else{
            $news = $news->whereHas('newscategory', function ($query) {
                $query->where('online_stat',1)->select(['id']);
            });
        }

        if (!is_null($title)){
			$news = $news->where('title', 'LIKE', '%'.$title.'%');
		}
		if (!is_null($created_by)){
			$news = $news->where('created_by', $created_by);
		}

		if (!is_null($table_sort_by_field) && !is_null($table_sort_by_order) && in_array($table_sort_by_field, ['title', 'posted_at', 'newscategory_id', 'created_by', 'status', 'contributor_id'])){
			$news = $news->orderBy($table_sort_by_field,$table_sort_by_order);
		}
		else{
			$news = $news->orderBy('id', 'DESC');
		}

        return $news->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator_news_date = Validator::make($request->news_date, [
            'formatted' => 'required_unless:ignore_date,|date',
        ]);
        if ($validator_news_date->fails()) {
            return response()->json($validator_news_date->getMessageBag()->toArray(), 400);
        }
        $validator = Validator::make($request->all(), [
            'news_time' => 'required|date_format:"H:i"',
            'show_writer_data' => 'boolean',
            'newscategory_id' => 'required|exists:newscategories,id',
            'title' => 'required',
            'status' => 'required|integer',
        ])->sometimes('contributor_id', 'exists:contributors,id', function($request) {
            return !is_null($request->contributor_id);
        });
        if ($validator->fails()) {
            return response()->json($validator->getMessageBag()->toArray(), 400);
        }
        \DB::beginTransaction();
		try{
            /* Insert New News */
            $news = News::create([
				'newscategory_id' => $request->newscategory_id,
                'parent_news_id' => $request->parent_news_id,
                'source_link' => !is_null($request->source_link) ? $request->source_link : '',
                'posted_at' => $request->ignore_date == 1 ? date('Y-m-d H:i:s') : date('Y-m-d', strtotime($request->news_date['formatted'])).' '.$request->news_time.':00',
                'news_type' => $request->news_type,
                'title' => $request->title,
                'subheader' => is_null($request->subheader) ? '' : $request->subheader,
                'content' => is_null($request->content) ? '' : $request->content,
                //'status' => (User::hasAccess('news.add.'.$this->arr_post_status[Input::get('status')])) ? Input::get('status'): Config::get('news.news_default_status'),
                'status' => $request->status,
                //'show_writer_data' => (User::hasAccess('news.add.approve')) ? ( ( Input::get('show_writer_data') !== '' )? 1 : 1 ) : 1 ,
                'show_writer_data' => $request->show_writer_data,
                'created_by' => 1 /*Auth::id()*/,
                //'contributor_id' => (User::hasAccess('news.contributor.add') && Input::get('contributor_id') !== "") ? Input::get('contributor_id') : null ,
                'contributor_id' => isset($request->contributor_id) && !is_null($request->contributor_id) ? $request->contributor_id : null,
                'hits' => 0,
			]);

            /*$news->update([
                'slug' => str_slug($request->news_title,'-').'/'.$news->id.'/'.date('Y/m/d')
            ]);*/

            if( count($request->images) ){
                $user_id = Auth::id();
                config(['news.image.temp_path.store' => 'assets/temp_user_image/'.$user_id.'/news/store/']);
                $date_path = $news->created_at->format('Y/m/');

                $temporary_large_path = config('news.image.temp_path.store').config('news.image.large.path');
                $large_path = config('news.image.path').config('news.image.large.path');
                $temporary_thumbnail_path = config('news.image.temp_path.store').config('news.image.thumbnail.path');
                $thumbnail_path = config('news.image.path').config('news.image.thumbnail.path');

                /* Insert New News Image */
                $slug = str_slug($news->id.'-'.$news->title,'-');
                if(!is_dir( $large_path.$date_path )){
                    File::makeDirectory( $large_path.$date_path , 0755, true);
                }
                if(!is_dir( $thumbnail_path.$date_path )){
                    File::makeDirectory( $thumbnail_path.$date_path , 0755, true);
                }
                if (count($request->images) === 1){
                    if( !isset($request->images[0]['image_caption']) || isset($request->images[0]['image_caption']) && $request->images[0]['image_caption'] === ''){
                        return response()->json([
                            'success' => false,
                            'error' => 'Teks gambar tidak boleh kosong' ,
                        ], 500);
                    }
                }

                foreach($request->images as $key => $image){
                    $news_image = $news->images()->save(
                        new NewsImage([
                            'image_file' => $date_path.$slug.($key ? '-'.$key : '').$image['file_ext'],
                            'image_caption' => isset($image['image_caption'])? $image['image_caption'] : '',
                            'cover' => isset($image['cover']) && $image['cover'] ? 1 : 0 ,
                            'old_image' => 0,
                            'source_image' => null,
                        ])
                    );
                    rename( $temporary_large_path.$image['file_name'], $large_path.$news_image->image_file);
                    rename( $temporary_thumbnail_path.$image['file_name'], $thumbnail_path.$news_image->image_file);
                }
			}
			\DB::commit();
			return response()->json([
				'success' => true,
				'data' => $news,
			]);
        }
		catch(\Exception $e){
			\DB::rollback();
			return response()->json([
				'success' => false,
				'error' => \Config::get('app.debug') ? $e->getMessage() : 'Opss.. Seperti nya terjadi kesalahan,silahkan coba kembali.' ,
			], 500);
		}

        return $request->all();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'success' => true,
            'data' => News::with(['newscategory', 'images', 'tags'])->findOrFail($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator_news_date = Validator::make($request->news_date, [
            'formatted' => 'required|date',
        ]);
        if ($validator_news_date->fails()) {
            return response()->json($validator_news_date->getMessageBag()->toArray(), 400);
        }
        $validator = Validator::make($request->all(), [
            'news_time' => 'required|date_format:"H:i:s"',
            'show_writer_data' => 'boolean',
            'newscategory_id' => 'required|exists:newscategories,id',
            'title' => 'required',
            'status' => 'required|integer',
        ])->sometimes('contributor_id', 'exists:contributors,id', function($request) {
            return !is_null($request->contributor_id);
        });
        if ($validator->fails()) {
            return response()->json($validator->getMessageBag()->toArray(), 400);
        }
        $news = News::findOrFail($id);
        \DB::beginTransaction();
		try{
            $news->update([
				'newscategory_id' => $request->newscategory_id,
                'parent_news_id' => $request->parent_news_id,
                'source_link' => !is_null($request->source_link) ? $request->source_link : '',
                'posted_at' => $request->ignore_date == 1 ? date('Y-m-d H:i:s') : date('Y-m-d', strtotime($request->news_date['formatted'])).' '.$request->news_time,
                'news_type' => $request->news_type,
                'title' => $request->title,
                'subheader' => is_null($request->subheader) ? '' : $request->subheader,
                'content' => is_null($request->content) ? '' : $request->content,
                //'status' => (User::hasAccess('news.add.'.$this->arr_post_status[Input::get('status')])) ? Input::get('status'): Config::get('news.news_default_status'),
                'status' => $request->status,
                //'show_writer_data' => (User::hasAccess('news.add.approve')) ? ( ( Input::get('show_writer_data') !== '' )? 1 : 1 ) : 1 ,
                'show_writer_data' => $request->show_writer_data,
                'created_by' => 1 /*Auth::id()*/,
                //'contributor_id' => (User::hasAccess('news.contributor.add') && Input::get('contributor_id') !== "") ? Input::get('contributor_id') : null ,
                'contributor_id' => isset($request->contributor_id) && !is_null($request->contributor_id) ? $request->contributor_id : null,
                //'hits' => 0,
			]);

            if( count($request->images) ){
                $user_id = Auth::id();
                config(['news.image.temp_path.update' => 'assets/temp_user_image/'.$user_id.'/news/update/'. $news->id.'/']);
                $date_path = $news->created_at->format('Y/m/');

                $temporary_large_path = config('news.image.temp_path.update').config('news.image.large.path');
                $large_path = config('news.image.path').config('news.image.large.path');
                $temporary_thumbnail_path = config('news.image.temp_path.update').config('news.image.thumbnail.path');
                $thumbnail_path = config('news.image.path').config('news.image.thumbnail.path');

                /* Insert New News Image */
                $slug = str_slug($news->id.'-'.$news->title,'-');
                if(!is_dir( $large_path.$date_path )){
                    File::makeDirectory( $large_path.$date_path , 0755, true);
                }
                if(!is_dir( $thumbnail_path.$date_path )){
                    File::makeDirectory( $thumbnail_path.$date_path , 0755, true);
                }
                if (count($request->images) === 1){
                    if( !isset($request->images[0]['image_caption']) || isset($request->images[0]['image_caption']) && $request->images[0]['image_caption'] === ''){
                        return response()->json([
                            'success' => false,
                            'error' => 'Teks gambar tidak boleh kosong' ,
                        ], 500);
                    }
                }

                foreach($request->images as $key => $image){
                    if( is_null($image['id']) ){
                        $news_image = $news->images()->save(
                            new NewsImage([
                                'image_file' => $date_path.$slug.($key ? '-'.$key : '').$image['file_ext'],
                                'image_caption' => isset($image['image_caption'])? $image['image_caption'] : '',
                                'cover' => isset($image['cover']) && $image['cover'] ? 1 : 0 ,
                                'old_image' => 0,
                                'source_image' => null,
                            ])
                        );
                        rename( $temporary_large_path.$image['file_name'], $large_path.$news_image->image_file);
                        rename( $temporary_thumbnail_path.$image['file_name'], $thumbnail_path.$news_image->image_file);
                    }
                    else{
                        $news_image = $news->images()->find($image['id'])->update([
                            'image_caption' => isset($image['image_caption'])? $image['image_caption'] : '',
                            'cover' => isset($image['cover']) && $image['cover'] ? 1 : 0 ,
                        ]);
                    }
                }

                foreach($request->deleted_images as $key => $deleted_image){
                    $news_image = $news->images()->find($deleted_image);
                    if( !is_null($news_image) ){
                        if( !file_exists( $large_path.$news_image->image_file ) )
                            unlink( $large_path.$news_image->image_file);
                        if( !file_exists( $thumbnail_path.$news_image->image_file ) )
                            unlink( $thumbnail_path.$news_image->image_file);
                        $news_image->delete();
                    }
                }
			}

			\DB::commit();
			return response()->json([
				'success' => true,
				'data' => $news,
			]);
        }
		catch(\Exception $e){
			\DB::rollback();
			return response()->json([
				'success' => false,
				'error' => \Config::get('app.debug') ? $e->getMessage() : 'Opss.. Seperti nya terjadi kesalahan,silahkan coba kembali.' ,
			], 500);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        \DB::beginTransaction();
		try{
            $large_path = config('news.image.path').config('news.image.large.path');
            $thumbnail_path = config('news.image.path').config('news.image.thumbnail.path');
            foreach($news->images as $image){
                if( file_exists( $large_path.$image->image_file ) )
                    unlink( $large_path.$image->image_file);
                if( file_exists( $thumbnail_path.$image->image_file ) )
                    unlink( $thumbnail_path.$image->image_file);
                $image->delete();
            }
            $news->tags()->delete();
            $news->delete();
			\DB::commit();
			return response()->json([
				'success' => true,
				'data' => $news,
			]);
        }
		catch(\Exception $e){
			\DB::rollback();
			return response()->json([
				'success' => false,
				'error' => \Config::get('app.debug') ? $e->getMessage() : 'Opss.. Seperti nya terjadi kesalahan,silahkan coba kembali.' ,
			], 500);
		}
    }
}
