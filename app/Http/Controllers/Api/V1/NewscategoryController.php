<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Newscategory;

class NewscategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->use_for === 'news' ){
            $newscategories = Newscategory::where('parent',null)->where('online_stat', (is_null($request->online_stat) ? 1 : $request->online_stat ))->active()->orderBy('lft')->get();
            $result = [];
            foreach($newscategories as $key => $newscategory){
                $k = [
                    'id' => (String)$newscategory->id,//ng-select must be string
                    'name' => $newscategory->name,
                    'parent' => $newscategory->parent,
                    'subCategories' => []
                ];
                foreach($newscategory->getDescendants() as $key => $descendant){
                    if( $descendant->status == 1 ){
                        $k['subCategories'][] = [
                            'id' => (String)$descendant->id,//ng-select must be string
                            'name' => '- '.$descendant->name,
                            'parent' => $newscategory->parent,
                            'subCategories' => []
                        ];
                    }
                }
                $result[] = $k;
            }
            return [
                'data' => $result
            ];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Newscategory::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
