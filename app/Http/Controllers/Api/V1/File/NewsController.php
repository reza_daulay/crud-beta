<?php

namespace App\Http\Controllers\Api\V1\File;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Image;
use File;
use Storage;

use App\News;

use App\Helpers\FileHelpers;

use Auth;

class NewsController extends Controller
{
    public function __construct()
	{
        $this->FileHelpers = new FileHelpers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = Auth::id();
        if( is_null($request->news_id) ){
            config(['news.image.temp_path.store' => 'assets/temp_user_image/'.$user_id.'/news/store/']);
        }
        else{
            News::findOrFail($request->news_id, ['id']);
            config(['news.image.temp_path.update' => 'assets/temp_user_image/'.$user_id.'/news/update/'. $request->news_id.'/']);
        }

        $thumbnail_path = config('news.image.temp_path.'.( is_null($request->news_id) ? 'store' : 'update' )).config('news.image.thumbnail.path');
        $files = $this->FileHelpers->readDir($thumbnail_path);
        $result = [];
        foreach( $files as $f ){
            $f = str_replace(config('app.main_path'), '', $f);
            $fileName = explode('/', $f);
            $filename = $fileName[count($fileName)-1];
            $result[] = [
                'id' => null,
                'news_id' => is_null($request->news_id) ? null : $request->news_id,
                'file_name' => $filename,
                'file_ext' => '.'.pathinfo($filename)['extension'],
                'small_url' => $this->FileHelpers->publicPath($f),
                'cover' => 0,
            ];
        }

        return response()->json([
            'data' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();
        config(['news.image.temp_path.store' => 'assets/temp_user_image/'.$user_id.'/news/store/']);

        $validator = Validator::make($request->all(), [
            'file' => 'required|image|max:'.config('news.image.large.size').'|dimensions:min_width='.config('news.image.large.width').',min_height='.config('news.image.large.height').'max_width='.config('news.image.large.width').',max_height='.config('news.image.large.height')
        ]);
        if ($validator->fails()) {
            return response()->json($validator->getMessageBag()->toArray(), 400);
        }

        $large_path = config('news.image.temp_path.store').config('news.image.large.path');
        if( !is_dir( $large_path ) ){
            File::makeDirectory( $large_path ,0755,true);
        }
        $thumbnail_path = config('news.image.temp_path.store').config('news.image.thumbnail.path');
        if( !is_dir( $thumbnail_path ) ){
            File::makeDirectory( $thumbnail_path ,0755,true);
        }

        $image = $request->file;
        $img = Image::make($request->file);
        $extension = $image->getClientOriginalExtension();
        $filename = time().'-'.rand(1,100).'-'.$user_id.'.'.$extension;

        // watermark
        if( !$request->get('with_watermark') ){
            $img->insert('assets/frontend/image/watermark_berita.png', 'center')->save($large_path.$filename,100);
            /* generate image thumbnail */
            Image::make($large_path.$filename)->resize(config('news.image.thumbnail.width'), config('news.image.thumbnail.height'))->save($thumbnail_path.$filename,100);
        }
        // no-watermark
        else{
            $img->save($large_path.$filename,100);
            /* generate image thumbnail */
            $img->resize(config('news.image.thumbnail.width'), config('news.image.thumbnail.height'))->save($thumbnail_path.$filename);
        }

        return response()->json([
            'success' => true,
            'data' => [
                'id' => null,
                'news_id' => null,
                'file_name' => $filename,
                'file_ext' => '.'.pathinfo($filename)['extension'],
                'small_url' => $this->FileHelpers->publicPath(str_replace(config('app.main_path'), '', $thumbnail_path.$filename)),
                'cover' => 0
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::findOrFail($id, ['id']);
        $user_id = Auth::id();
        config(['news.image.temp_path.update' => 'assets/temp_user_image/'.$user_id.'/news/update/'. $news->id.'/']);

        $validator = Validator::make($request->all(), [
            'file' => 'required|image|max:'.config('news.image.large.size').'|dimensions:min_width='.config('news.image.large.width').',min_height='.config('news.image.large.height').'max_width='.config('news.image.large.width').',max_height='.config('news.image.large.height')
        ]);
        if ($validator->fails()) {
            return response()->json($validator->getMessageBag()->toArray(), 400);
        }

        $large_path = config('news.image.temp_path.update').config('news.image.large.path');
        if( !is_dir( $large_path ) ){
            File::makeDirectory( $large_path ,0755,true);
        }
        $thumbnail_path = config('news.image.temp_path.update').config('news.image.thumbnail.path');
        if( !is_dir( $thumbnail_path ) ){
            File::makeDirectory( $thumbnail_path ,0755,true);
        }

        $image = $request->file;
        $img = Image::make($request->file);
        $extension = $image->getClientOriginalExtension();
        $filename = time().'-'.rand(1,100).'-'.$user_id.'.'.$extension;

        // watermark
        if( !$request->get('with_watermark') ){
            $img->insert('assets/frontend/image/watermark_berita.png', 'center')->save($large_path.$filename,100);
            /* generate image thumbnail */
            Image::make($large_path.$filename)->resize(config('news.image.thumbnail.width'), config('news.image.thumbnail.height'))->save($thumbnail_path.$filename,100);
        }
        // no-watermark
        else{
            $img->save($large_path.$filename,100);
            /* generate image thumbnail */
            $img->resize(config('news.image.thumbnail.width'), config('news.image.thumbnail.height'))->save($thumbnail_path.$filename);
        }

        return response()->json([
            'success' => true,
            'data' => [
                'id' => null,
                'news_id' => null,
                'file_name' => $filename,
                'file_ext' => '.'.pathinfo($filename)['extension'],
                'small_url' => $this->FileHelpers->publicPath(str_replace(config('app.main_path'), '', $thumbnail_path.$filename)),
                'cover' => 0
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  String  $idOrUrl
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $idOrUrl)
    {
        $user_id = Auth::id();
        if( is_null($request->news_id) ){
            config(['news.image.temp_path.store' => 'assets/temp_user_image/'.$user_id.'/news/store/']);
        }
        else{
            News::findOrFail($request->news_id, ['id']);
            config(['news.image.temp_path.update' => 'assets/temp_user_image/'.$user_id.'/news/update/'. $request->news_id.'/']);
        }
        if( !is_numeric($idOrUrl) ){
            $large_image = config('news.image.temp_path.'.( is_null($request->news_id) ? 'store' : 'update' )).config('news.image.large.path').$idOrUrl;
            $thumbnail_image = config('news.image.temp_path.'.( is_null($request->news_id) ? 'store' : 'update' )).config('news.image.thumbnail.path').$idOrUrl;
            if( !file_exists( $large_image ) )
                abort(404);
            File::delete($large_image, $thumbnail_image);
            return response()->json([
                'success' => true,
                'filename' => $idOrUrl
            ]);
        }
    }
}
