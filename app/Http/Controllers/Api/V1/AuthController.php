<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

function asep($path, &$array=array(), $value=null) {
    $temp = &$array;

    foreach($path as $key) {
        $temp =& $temp[$key];
    }
    $temp = $value;
    return $temp;
}

class AuthController extends Controller
{
    /**
     * Display the specified roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function roles()
    {
        //$roles = Auth::user()->usergroup->roles;
        $roles = \App\User::first()->usergroup->roles;
        $data = [];
        $i = 0;
        foreach($roles as $key => $role){
            $arrNames = explode('.', $role->name);
            if( isset($data[$arrNames[0]]) && in_array($arrNames[1], $data[$arrNames[0]]) )
                continue;
            else{
                if( isset($arrNames[2]) )
                    $data[$arrNames[0]][ $arrNames[1]][] = $arrNames[2];
                else
                    $data[$arrNames[0]][] = $arrNames[1];
            }
        }
        return response()->json([
            'data' => $data
        ]);
    }

}
