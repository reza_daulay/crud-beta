<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at', 'created_at', 'updated_at' ,'posted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'newscategory_id', 'parent_news_id', 'source_link', 'posted_at', 'news_type', 'title', 'subheader', 'content', 'status', 'show_writer_data', 'created_by', 'contributor_id', 'hits'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
     protected $guarded = [
        'id'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'news_date', 'news_time', 'status_text'
    ];

	public function changeConnection($conn)
	{
		$this->connection = $conn;
	}

	public function newscategory()
    {
        return $this->belongsTo('App\Newscategory');
    }

	public function writer()
    {
        return $this->belongsTo('App\User','created_by')->withTrashed();
    }

	public function images()
    {
        return $this->hasMany('App\NewsImage')->orderBy('cover','desc');
    }

	public function imageCover()
    {
        return $this->hasOne('App\NewsImage')->where('cover', 1);
    }

	public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

	public function getNewsDateAttribute()
    {
        return [
            'formatted' => $this->posted_at->format('Y-m-d'),
            'date' => [
                'year' => $this->posted_at->format('Y'),
                'month' => $this->posted_at->format('n'),
                'day' => $this->posted_at->format('j'),
            ]
        ];
        //return $this->posted_at->format('Y-m-d');
    }
    
	public function getNewsTimeAttribute()
    {
        return $this->posted_at->format('H:i:s');
    }
    
	public function getStatusTextAttribute()
    {
        if($this->status == 1)
            return 'Publis';
        else if($this->status == 2)
            return 'Tunda';
        else if($this->status == 3)
            return 'Ditolak';
    }
}
