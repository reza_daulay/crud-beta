<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Newscategory extends \Baum\Node
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref_title', 'temp_name', 'name', 'parent', 'depth', 'lft', 'rgt', 'status', 'online_stat', 'sort', 'created_by'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'created_by', 'temp_name'
    ];

    // 'parent_id' column name
    protected $parentColumn = 'parent';

    // 'lft' column name
    protected $leftColumn = 'lft';

    // 'rgt' column name
    protected $rightColumn = 'rgt';

    // 'depth' column name
    protected $depthColumn = 'depth';


	public function changeConnection($conn)
	{
		$this->connection = $conn;
	}

	public function news()
    {
        return $this->hasMany('App\News');
    }

	public function getChilds()
    {
        return $this->hasMany('App\Newscategory','parent');
    }

	public function getActiveChilds()
    {
        return $this->hasMany('App\Newscategory','parent')->where('status',1);
    }

	public function getParent()
    {
        return $this->belongsTo('App\Newscategory','parent');
    }

	public function scopeActive($query)
    {
        return $query->where('status',1);
    }
}
