<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Helpers\FileHelpers;

class NewsImage extends Model
{
    /*public function __construct()
	{
        $this->FileHelpers = new FileHelpers;
    }*/
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['news_id', 'cover', 'image_file', 'image_caption', 'old_image', 'source_image'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['image_file', 'old_image', 'source_image', 'deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['file_name', 'file_ext', 'small_url', 'big_url'];

	public function changeConnection($conn)
	{
		$this->connection = $conn;
	}

	public function news()
    {
        return $this->belongsTo('App\News');
    }
    
	public function scopeCover($query)
    {
        return $query->where('cover',1);
    }

	public function getFileNameAttribute()
    {
        $fileName = explode('/', $this->image_file);
        $filename = $fileName[count($fileName)-1];
        return $filename;
    }

	public function getFileExtAttribute()
    {
        $fileName = explode('/', $this->image_file);
        $filename = $fileName[count($fileName)-1];
        return '.'.pathinfo($filename)['extension'];
    }

	public function publicPath($path)
    {
        $fileHelpers = new FileHelpers;
        return $fileHelpers->publicPath($path);
    }

	public function getBigUrlAttribute()
    {
        return $this->publicPath(config('news.image.path').config('news.image.large.path').$this->image_file);
    }

	public function getSmallUrlAttribute()
    {
        return $this->publicPath(config('news.image.path').config('news.image.thumbnail.path').$this->image_file);
    }
}
