<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'tag_count'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
     protected $guarded = [];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['tag_count', 'created_at', 'updated_at'];
}
