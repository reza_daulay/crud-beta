<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <base href="{{ url('/') }}">

    <api-url content="{{ url('api/v1') }}/" />
    <base-domain content="{{ url('') }}/" />
    <base-asset content="{{ url('assets/backend') }}/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="shortcut icon" href="{{ asset('assets/backend/assets/favicons/favicon.ico') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ asset('assets/backend/assets/favicons/apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/backend/assets/favicons/apple-touch-icon-57x57.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/backend/assets/favicons/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/backend/assets/favicons/apple-touch-icon-76x76.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/backend/assets/favicons/apple-touch-icon-114x114.png') }}" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/backend/assets/favicons/apple-touch-icon-120x120.png') }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/backend/assets/favicons/apple-touch-icon-144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/backend/assets/favicons/apple-touch-icon-152x152.png') }}" />
    <link href="{{ asset('assets/backend/assets/twitter-bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/assets/css/external.css?v=1') }}" rel="stylesheet">
    
    <!-[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]->
</head>
<body>
    <my-app>Loading...</my-app>
    <script type="text/javascript" src="{{ asset('assets/backend/assets/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/inline.bundle.js?v=1') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/polyfills.bundle.js?v=1') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/styles.bundle.js?v=1') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/vendor.bundle.js?v=1') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/main.bundle.js?v=1') }}"></script>
</body>
</html>
