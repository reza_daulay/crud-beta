import { AnManagePage } from './app.po';

describe('an-manage App', () => {
  let page: AnManagePage;

  beforeEach(() => {
    page = new AnManagePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
