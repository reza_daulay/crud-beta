import { NgModule }             from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { AuthGuardService } from './services/authGuard'

//import { LoginComponent }   from './components/auth/login
import { DashboardComponent }     from './components/dashboard'
import { IndexNewsComponent }     from './components/news/index'
import { CreateNewsComponent }     from './components/news/create'
import { EditNewsComponent }     from './components/news/edit'

const routes: Routes = [
  { path: '', component: DashboardComponent, pathMatch: 'full', data: { 'name' : 'index' } },
  { path: 'news',  component: IndexNewsComponent, data: { 'name' : 'news.view' }, canActivate: [AuthGuardService] },
  { path: 'news/create',  component: CreateNewsComponent, data: { 'name' : 'news.add' }, canActivate: [AuthGuardService], },
  { path: 'news/:id/edit',  component: EditNewsComponent, data: { 'name' : 'news.edit' }, canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { useHash: true }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
