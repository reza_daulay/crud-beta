import { NgModule, enableProdMode }      from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import {
  HttpModule, Http,
  XHRBackend, RequestOptions
} from '@angular/http'
import { ErrorNotifierService } from './services/errorNotifier'
import { CustomHttpService } from './services/customHttp'
import { AppRequestOptions } from './services/appRequestOptions'

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { AppRoutingModule } from './app-routing.module'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown'
import { AccordionModule } from 'ngx-bootstrap/accordion'
import { ModalModule } from 'ngx-bootstrap/modal'
import { TypeaheadModule } from 'ngx-bootstrap/typeahead'
import { PaginationModule } from 'ngx-bootstrap/pagination'
import { CarouselModule } from 'ngx-bootstrap/carousel'
import { NguiDatetimePickerModule } from '@ngui/datetime-picker'
import { TextMaskModule } from 'angular2-text-mask'
import { UiSwitchModule } from 'ng2-ui-switch'
import { SelectModule } from 'ng-select'
import { TagInputModule } from 'ng2-tag-input'
import { FileUploadModule } from 'ng2-file-upload'
import { ScrollToModule } from 'ng2-scroll-to-el'
//import { MyDatePickerModule } from 'mydatepicker'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker'

import { CKEDITOR } from './components/ckeditor'

import { MenuAccordionComponent } from './components/menu-accordion'
import { SidebarWrapperComponent } from './components/sidebar-wrapper'

import { AppComponent }     from './components/app'
import { DashboardComponent }     from './components/dashboard'

import { IndexNewsComponent }     from './components/news/index'
import { CreateNewsComponent }     from './components/news/create'
import { EditNewsComponent }     from './components/news/edit'
//import { LoginComponent }   from './components/auth/login'

import { DomService } from './services/dom'
import { AuthGuardService } from './services/authGuard'
import { LocalStorageService } from './services/localStorage'
import { UserService } from './services/user'
import { RoleService } from './services/role'
import { TagService } from './services/tag'
import { NewsService } from './services/news'
import { NewscategoryService } from './services/newscategory'

import { MatchHeightDirective } from './directives/match-height'


enableProdMode()

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    ScrollToModule.forRoot(),
    PaginationModule.forRoot(),
    CarouselModule.forRoot(),
    NguiDatetimePickerModule,
    TextMaskModule,
    UiSwitchModule,
    SelectModule,
    TagInputModule,
    BrowserAnimationsModule,
    FileUploadModule,
    //MyDatePickerModule,
    NgxMyDatePickerModule,
  ],
  declarations: [
    CKEDITOR,
    AppComponent,
    MenuAccordionComponent,
    SidebarWrapperComponent,
    MatchHeightDirective,
    DashboardComponent,
    IndexNewsComponent,
    CreateNewsComponent,
    EditNewsComponent,
  ],
  providers: [
    /*ErrorNotifierService,
    {
      provide: Http,
      useFactory: (backend: XHRBackend, defaultOptions: RequestOptions, errorNotifier: ErrorNotifierService) => {
        return new CustomHttpService(backend, defaultOptions, errorNotifier);
      },
      deps: [ XHRBackend, RequestOptions, ErrorNotifierService ]
    },
    {
      provide: RequestOptions, useClass: AppRequestOptions
    },*/
    DomService,
    AuthGuardService,
    LocalStorageService,
    UserService,
    RoleService,
    TagService,
    NewsService,
    NewscategoryService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
