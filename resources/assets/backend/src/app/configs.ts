export const Configs = {
  'baseDomain' : document.getElementsByTagName('base-domain')[0].getAttribute('content'),
  'baseAsset' : document.getElementsByTagName('base-asset')[0].getAttribute('content'),
  'apiUrl' : document.getElementsByTagName('api-url')[0].getAttribute('content')
};
