export class UserObject {
  id: number
  first_name: string
  last_name: string
  usergroup_id: number
}
