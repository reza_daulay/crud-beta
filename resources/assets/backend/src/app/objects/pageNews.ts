import { NewsObject } from './news'

export class PageNewsObject {
  current_page: number
  data: NewsObject[]
  from: number
  last_page: number
  next_page_url: string
  path: string
  per_page: number
  prev_page_url: string
  to: number
  total: number
}