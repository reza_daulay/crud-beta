import { NewsImageObject } from './newsImage'

export class NewsObject {
  id: number = null
  ignore_date: boolean = false
  newscategory_id: string
  parent_news_id: number = null
  posted_at: string
  news_date: string
  news_time: string
  title: string
  subheader: string = null
  content: string = null
  source_link: string = null
  news_type: number = 1
  status: number = 1
  show_writer_data: boolean = true
  hits: number
  created_by: number
  contributor_id: number = null
  tags: any[]
  created_at: string
  updated_at: string
  images: NewsImageObject[]
  image_cover: NewsImageObject
  deleted_images: number[]
}
