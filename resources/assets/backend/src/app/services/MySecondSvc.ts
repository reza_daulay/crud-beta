import { Injectable }              from '@angular/core';
import { RoleService } from './role'

@Injectable()
export class MySecondSvc {
  constructor(private _firstSvc:RoleService) {}
  getValue() {
    return this._firstSvc.getRoles();
  }
}