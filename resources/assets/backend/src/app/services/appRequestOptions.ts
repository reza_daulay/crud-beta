import { Injectable, Inject, InjectionToken} from '@angular/core';
import {BaseRequestOptions, RequestOptions, RequestOptionsArgs} from '@angular/http';
import { Configs } from '../configs';


export class AppRequestOptions extends BaseRequestOptions {

  merge(options?:RequestOptionsArgs):RequestOptions {
    options.url = Configs.apiUrl + options.url;
    return super.merge(options);
  }
}