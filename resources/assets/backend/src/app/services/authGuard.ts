import { Injectable }       from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
}                           from '@angular/router';

import { RoleService } from '../services/role'

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private roleService: RoleService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let url: string = state.url;
    return this.checkRole(route.data.name)
  }

  checkRole(routeName): boolean {
    this.roleService.getRoles().subscribe(roles => {
      if ( this.roleService.hasRole(roles, routeName) )  { 
        return true;
      }
      else { 
        this.router.navigate(['/']);
        window.location.reload() 
      }
    })
    return true
  }
}