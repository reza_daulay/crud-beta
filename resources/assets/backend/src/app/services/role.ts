import {Injectable} from '@angular/core'
import {Http, Headers} from '@angular/http'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/observable/of' //proper way to import the 'of' operator
import 'rxjs/add/operator/share'
import 'rxjs/add/operator/map'
import { RoleObject } from '../objects/role'
import { Configs } from '../configs'
import 'rxjs/add/operator/toPromise'

@Injectable()
export class RoleService {
  private headers = new Headers({'Content-Type': 'application/json'})
  private roleUrl = Configs.apiUrl+'roles'

  private userRoles: RoleObject[]
  private observable: Observable<any>

  constructor(private http:Http) {}

  getRoles(): Observable<RoleObject[]> {
    if(this.userRoles) {
      // if `data` is available just return it as `Observable`
      return Observable.of(this.userRoles)
    } else if(this.observable) {
      // if `this.observable` is set then the request is in progress
      // return the `Observable` for the ongoing request
      return this.observable
    } else {
      // example header (not necessary)
      let headers = new Headers()
      headers.append('Content-Type', 'application/json')
      // create the request, store the `Observable` for subsequent subscribers
      this.observable = this.http.get(this.roleUrl, {
        headers: headers
      })
      .map(response =>  {
        // when the cached data is available we don't need the `Observable` reference anymore
        this.observable = null

        if(response.status == 400) {
          return "FAILURE"
        } else if(response.status == 200) {
          this.userRoles = response.json().data
          return this.userRoles
        }
        // make it shared so more than one subscriber can get the result
      })
      .share()
      return this.observable
    }
  }

  hasRole(roles:any[], roleName:string): any {
    let roleNameSplit = roleName.split('.')
    let data:string
    if( roleNameSplit.length === 3 )
      data = typeof roles[roleNameSplit[0]][roleNameSplit[1]][roleNameSplit[2]] !== 'undefined' ? roles[roleNameSplit[0]][roleNameSplit[1]][roleNameSplit[2]] : []
    else if( roleNameSplit.length === 2 )
      data = typeof roles[roleNameSplit[0]][roleNameSplit[1]] !== 'undefined' ? roles[roleNameSplit[0]][roleNameSplit[1]] : []
    else
      data = typeof roles[roleNameSplit[0]] !== 'undefined' ? roles[roleNameSplit[0]] : []
    return data.length ? true : false
  }
}