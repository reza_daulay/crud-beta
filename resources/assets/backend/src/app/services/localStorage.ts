export class LocalStorageService {
  public save(name:string, val:any): void {
    window.localStorage.setItem(name, JSON.stringify(val))
  }

  public get(name:string): any {
    if( window.localStorage.getItem(name) !== null )
      return JSON.parse( window.localStorage.getItem(name) )
    else
      return null
  }

  public delete(name:string): void {
    window.localStorage.removeItem(name)
  }
}
