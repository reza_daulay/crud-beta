import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';

import { Configs } from '../configs';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { TagObject } from '../objects/tag'

@Injectable()
export class TagService {
  private tagUrl = Configs.apiUrl+'tags';  // URL to web api
  constructor (private http: Http) {}
  
  getTags(keyword): Observable<TagObject[]> {
    return this.http.get(this.tagUrl+'?q='+keyword).map(this.extractData).catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    
    return body.data.map(item => item.title) || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}