import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Configs } from '../configs';

import 'rxjs/add/operator/toPromise';

import { UserObject } from '../objects/user'

@Injectable()
export class UserService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private userUrl = Configs.apiUrl+'users';

  constructor(private http: Http) { }

  getUserList(): Promise<UserObject[]> {
    return this.http.get(this.userUrl+'?use_for=list')
               .toPromise()
               .then(response => response.json().data as UserObject[])
               .catch(this.handleError);
  }

  logOut(): Promise<any> {
    return this.http.get(Configs.baseDomain+'logout')
               .toPromise()
               .then(response => response.json().data)
               .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
      return Promise.reject({
        'status' : error.status,
        'body' : typeof error._body !== 'undefined' ? JSON.parse(error._body) : null,
      })
  }
}
