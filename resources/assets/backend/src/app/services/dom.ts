export class DomService {

  public savingDraft: boolean = false
  public savingData: boolean = false
  public loadingData: boolean = false
  public validationError: any = null

}
