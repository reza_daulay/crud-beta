import { Injectable }    from '@angular/core'
import { Headers, Http } from '@angular/http'
import { Configs } from '../configs'

import 'rxjs/add/operator/toPromise'

import { PageNewsObject } from '../objects/pageNews'
import { NewsObject } from '../objects/news'
import { NewsImageObject } from '../objects/newsImage'

@Injectable()
export class NewsService {

  private headers = new Headers({'Content-Type': 'application/json'})
  private newsUrl = Configs.apiUrl+'news'
  private newsImageUrl = Configs.apiUrl+'files/news'

  constructor(private http: Http) { }

  getTemporaryNewsImages(news_id: string = null): Promise<NewsImageObject[]> {
    return this.http.get(this.newsImageUrl+'?news_id='+( news_id === null ? '' : news_id ))
    .toPromise()
    .then(response => response.json().data as NewsImageObject[])
    .catch(this.handleError)
  }

  deleteNewsImage(id: number, url:string=null, news_id: number = null): Promise<NewsImageObject[]> {
    return this.http.delete(this.newsImageUrl+'/'+( id === null ? url : id )+'?news_id='+( news_id === null ? '' : news_id ), {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(this.handleError)
  }
  

  index(arrParams: any[] = []): Promise<PageNewsObject> {
    let params = []
    Object.keys(arrParams).map(function(k, i) {
      params.push(k+'='+arrParams[k])
    })
    return this.http.get(this.newsUrl+( params.length ? '?'+params.join('&') : '' ))
      .toPromise()
      .then(res => res.json() as PageNewsObject)
      .catch(this.handleError)
  }

  create(data: any): Promise<NewsObject> {
    return this.http
      .post(this.newsUrl, data, {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as NewsObject)
      .catch(this.handleError)
  }

  update(data: any): Promise<NewsObject> {
    return this.http
      .put(this.newsUrl+'/'+data.id, data, {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as NewsObject)
      .catch(this.handleError)
  }

  delete(id: number): Promise<NewsObject> {
    return this.http
      .delete(this.newsUrl+'/'+id, {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as NewsObject)
      .catch(this.handleError)
  }

  getNewsTypeText(newsType) : string {
    let NewsTypeText : string[] = ['', 'Foto Dan Gambar', 'Hanya Teks', 'Hanya Foto' , 'Video Dan Teks' , 'Video Dan Gambar', 'Video, Teks & Foto']
    return NewsTypeText[newsType]
  }

  getNewsType(content, newsImagesLength) : number {
    //1 = 'text + image', 2 = 'texttype', 3 = 'imagetype' , 4 = 'video + text' , 5 = 'video + image', 6 = 'video + text + image'
          
    let newsType = 2
    let hasVideo = false
    content = typeof content === undefined || content === null ? '' : content
    let hasText = content.length > 100 ? true : false

    let div = document.createElement('div')
    div.classList.add('hidden')
    div.innerHTML = content

    let iframes = div.getElementsByTagName('iframe')
    let key = /youtube.com/
    for (let i = 0; i != iframes.length; ++i) {
      let match = iframes[i].src.search(key)
      if(match != -1) {
          hasVideo = true
          break
      }
    }
    if (newsImagesLength){
      if( hasVideo )
        newsType = hasText ? 6: 5
      else
        newsType = hasText ? 1: 3
    }
    return newsType
  }

  getNews(id: number): Promise<NewsObject> {
    const url = `${this.newsUrl}/${id}`
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as NewsObject)
      .catch(this.handleError)
  }

  private handleError(error: any): Promise<any> {
    //console.error('An error occurred', error)
    //return Promise.reject(error.message || error)

      return Promise.reject({
        'status' : error.status,
        'body' : typeof error._body !== 'undefined' ? JSON.parse(error._body) : null,
      })
  }
}
