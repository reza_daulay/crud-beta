import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Configs } from '../configs';

import 'rxjs/add/operator/toPromise';

import { NewscategoryObject } from '../objects/newscategory'

@Injectable()
export class NewscategoryService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private newscategoryUrl = Configs.apiUrl+'newscategories';

  constructor(private http: Http) { }

  getNewscategories(useFor: string = 'news'): Promise<NewscategoryObject[]> {
    return this.http.get(this.newscategoryUrl+'?use_for='+useFor)
               .toPromise()
               .then(response => response.json().data as NewscategoryObject[])
               .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
      return Promise.reject({
        'status' : error.status,
        'body' : typeof error._body !== 'undefined' ? JSON.parse(error._body) : null,
      })
  }
}
