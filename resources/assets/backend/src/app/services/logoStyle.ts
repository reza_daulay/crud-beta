import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Configs } from '../configs';

import 'rxjs/add/operator/toPromise';

import { LogoStyle } from '../objects/logoStyle';

@Injectable()
export class LogoStyleService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private logoStyleUrl = Configs.apiUrl+'briefs/style-pref-designs';  // URL to web api

  constructor(private http: Http) { }

  getLogoStyles(): Promise<LogoStyle[]> {
    return this.http.get(this.logoStyleUrl)
               .toPromise()
               .then(response => response.json().data as LogoStyle[])
               .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
