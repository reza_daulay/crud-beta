import { Component, OnInit } from '@angular/core'
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload'
import { ActivatedRoute, Params } from '@angular/router'
import { IMyDpOptions } from 'mydatepicker'
import { Configs } from '../../configs'

import { TagObject } from '../../objects/tag'
import { NewsObject } from '../../objects/news'
import { NewsImageObject } from '../../objects/newsImage'

import { DomService } from '../../services/dom'
import { LocalStorageService } from '../../services/localStorage'
import { TagService } from '../../services/tag'
import { NewsService } from '../../services/news'
import { NewscategoryService } from '../../services/newscategory'
import { ScrollToService } from 'ng2-scroll-to-el'

import { CKEDITOR } from '../ckeditor'

@Component({
  selector: 'edit-news',
  templateUrl: '../../templates/news/edit.html',
  styleUrls: [ '../../styles/news/edit.css' ]
})

export class EditNewsComponent implements OnInit {
	
  constructor(
    private route: ActivatedRoute, private domService: DomService, private localStorage: LocalStorageService, private newsService: NewsService, private newscategoryService: NewscategoryService, private tagService: TagService, private scrollService: ScrollToService) {
    }

  private mask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/, ':', /[0-5]/, /[0-9]/]

  private initDatas = {
    newscategory_id: null,
    successMessage: false,
    errorLoad: false,
    news_id: null,
    errorMessages: [],
    writingRole: null,
    contributor_ids : [],
    newscategory_ids : [],
    news_tags : [],
    uploader_errors : [],
  }

  private news: NewsObject

  private myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'yyyy-mm-dd',
        //inline: false,
        //disableSince: {year: this.yyyy, month: this.mm, day: this.dd}
  };

  //private newsImages: NewsImageObject[];
  private getTemporaryNewsImages(news_id): void {
    this.newsService
        .getTemporaryNewsImages(news_id)
        .then(newsImages => {
          let images = [];
          /*get uploaded news images*/
          for(let i=0; i < newsImages.length; i++){
            this.news.images.push(newsImages[i])
          }
        });
        
    
  }
  
  private uploader:FileUploader = new FileUploader({url: '', method: 'POST'});

  
  private requestTagItems = (q: string) => {
        return this.tagService.getTags(q)
  }

  private deleteNewsImage(newsImage: NewsImageObject): void {
    if( newsImage.id === null ){
      if (confirm('Hapus gambar ini ?')) {
        this.newsService
        .deleteNewsImage(newsImage.id, encodeURIComponent(newsImage.file_name), this.news.id)
        .then(() => {
          this.news.images = this.news.images.filter(h => h !== newsImage);
        });
      }
    }
    else{
      this.news.deleted_images.push(newsImage.id)
      this.news.images = this.news.images.filter(h => h !== newsImage);
    }
    if( this.news.images.length === 1){
      this.news.images[0].cover = true
    }    
  }

  private buttonImageClass(isCover): string {
    if( isCover )
      return 'btn-primary';
    else 
      return 'btn-default';
  }

  private selectACover(image): void {
    for(let i=0; i < this.news.images.length; i++)
      this.news.images[i].cover = false
    image.cover = true
  }

  private onChangeContent(val: string) {
    this.news.content = val
  }

  private onChangeImageContent(val: string, model: any) {
    model.image_caption = val
  }

  private updateNews(){
    this.initDatas.errorMessages = []
    if( this.news.images.length === 1 && ( typeof this.news.images[0].image_caption === 'undefined' || this.news.images[0].image_caption === null ) ) {
      this.initDatas.errorMessages.push('Caption gambar tidak boleh kosong')
      return this.scrollService.scrollTo('.box-title')
    }
    if( this.news.images.length ){
      let hasCover = false
      for(let i=0; i < this.news.images.length; i++){
        if( typeof this.news.images[0].cover !== 'undefined' || this.news.images[0].cover )
          hasCover = true
      }
      if( !hasCover ){
        this.initDatas.errorMessages.push('Silahkan pilih satu cover gambar')
        return this.scrollService.scrollTo('.box-title')
      }
    }

    this.news.news_type = this.newsService.getNewsType(this.news.content, this.news.images.length)
    this.domService.savingData = true
    let errorMessages = []
    this.newsService.update(this.news)
    .then(news => {
      this.initDatas.successMessage = true
      setTimeout(()=>{ this.initDatas.successMessage = false }, 4000)
      this.domService.savingData = false
      this.scrollService.scrollTo('.box-title')
      this.news.deleted_images = []
    })
    .catch((error) => {
      if( error.status === 500){
        errorMessages.push(error.body.error)
      }
      else{
        Object.keys(error.body).map(function(key, index) {
          errorMessages.push(error.body[key])
        });
      }
        this.initDatas.errorMessages =  errorMessages
        this.domService.savingData = false
        this.scrollService.scrollTo('.box-title')
    });

  }

  private generateNewscategoryList():void{
    this.newscategoryService
        .getNewscategories()
        .then(newscategories => {
          let newscategory_ids = []
          for(let i=0; i < newscategories.length; i++){
            newscategory_ids.push({
              value: newscategories[i].id,
              label: newscategories[i].name,
            })
            for(let ic=0; ic < newscategories[i].subCategories.length; ic++){
              newscategory_ids.push({
              value: newscategories[i].subCategories[ic].id,
              label: newscategories[i].subCategories[ic].name,
            })
            }
          }
          this.initDatas.newscategory_ids = newscategory_ids
          setTimeout(() => {
            this.news.newscategory_id = this.initDatas.newscategory_id
          });
    });
  }

  private getNews(){
    this.initDatas.errorLoad = false
    this.newsService.getNews(this.initDatas.news_id)
    .then(news => {
      this.news = news
      this.news.deleted_images = []
      this.newsService.getNewsType(this.news.content, this.news.images.length)
      this.news.newscategory_id = this.news.newscategory_id.toString()   
      this.initDatas.newscategory_id = this.news.newscategory_id.toString()

      this.generateNewscategoryList()
      this.getTemporaryNewsImages(this.news.id)
      this.uploader.options.url = Configs.apiUrl+'files/news/'+ this.news.id
    })
    .catch((error) => {
      this.initDatas.errorLoad = true
    })
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.initDatas.news_id = params['id']
      this.getNews()
    })

	  this.initDatas.writingRole = '<p>#1: Menulis dengan jujur.<br /><br />#2: Perhatikan Tanda Baca.</p><br />'
    this.initDatas.contributor_ids = [
        {value: null, label: 'Pilih Salah Satu'},
        {value: '1', label: 'Art3mis'},
        {value: '2', label: 'Daito'},
        {value: '3', label: 'Parzival'},
        {value: '4', label: 'Shoto'}
    ]
    
    this.uploader.onAfterAddingFile = (item) => {item
      item.upload()
    }

    this.uploader.onBeforeUploadItem = (item) => {
      item.withCredentials = false
    }
    
    this.uploader.onErrorItem = (item, response) => {
      this.initDatas.uploader_errors.push(JSON.parse(response).file)
      item.remove()
    }
    this.uploader.onSuccessItem = (item, response) => {
      this.initDatas.uploader_errors = []
      let image = JSON.parse(response).data
      if( !this.news.images.length )
        image.isCover = true
      this.news.images.push(image)
      setTimeout(()=>{ this.uploader.removeFromQueue(item) }, 2000)      
    }
    
  }

}
