import { Component, ViewChild, OnInit } from '@angular/core'
import { ModalDirective } from 'ngx-bootstrap/modal'

import { Router, NavigationExtras, NavigationEnd, ActivatedRoute, Params } from '@angular/router'
import { IMyDpOptions } from 'mydatepicker'
import { Configs } from '../../configs'

import { PageNewsObject } from '../../objects/pageNews'
import { NewsObject } from '../../objects/news'

import { DomService } from '../../services/dom'
import { UserService } from '../../services/user'
import { NewsService } from '../../services/news'
import { NewscategoryService } from '../../services/newscategory'

@Component({
  selector: 'index-news',
  templateUrl: '../../templates/news/index.html',
  styleUrls: [ '../../styles/news/index.css' ]
})

export class IndexNewsComponent implements OnInit {
	
  constructor(
    private router: Router, private activatedRoute: ActivatedRoute, private domService: DomService, private userService: UserService, private newsService: NewsService, private newscategoryService: NewscategoryService) {
      this.router.events.subscribe((val) => {
          if(val instanceof NavigationEnd)
            this.getNews()
      })
      this.activatedRoute.queryParams.subscribe((params: Params) => {
          if( !this.initDatas.hasLoaded ){
            if( Object.keys(params).length ){
              for (let k in params) {
                if( k === 'table_sort_by_field' )
                  this.table_sort_by_field = params[k]
                else if( k === 'table_sort_by_order' )
                  this.table_sort_by_order = params[k]
                else if( k === 'title' )
                  this.searchForm[k] = params[k]
                else if( k === 'newscategory_id' || k === 'created_by' )
                  this.initDatas[k] = params[k]
                else if( k === 'start_date' || k === 'end_date' ){
                  const theDay = new Date(params[k])
                  this.searchForm[k] = { date: { year: theDay.getFullYear(), month: theDay.getMonth()+1, day: theDay.getDate() }, formatted : params[k] }
                }
                this.manipulateQueryParams([{ key : k, value : params[k]}])
              }
            }
            else{
              this.queryParams = []
            }
          }
      })
    }
  
  private configs = Configs
  private pageNews: PageNewsObject
  private queryParams: any[] = []
  private table_sort_by_field = 'posted_at'
  private table_sort_by_order = 'asc'
  private searchForm = {
    start_date : null,
    end_date : null,
    newscategory_id : null,
    title : null,
    created_by : null,
  }

  @ViewChild('newsModal') private newsModal:ModalDirective;
  private showNews: NewsObject

  private myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'yyyy-mm-dd',
  }

  private initDatas = {
    errorLoad : false,
    hasLoaded : false,
    newscategory_ids : [],
    newscategory_id : null,
    writers : [],
    created_by : null
  }

  public currentPage: number = 1

  private generateNewscategoryList():void{
    this.newscategoryService
        .getNewscategories()
        .then(newscategories => {
          let newscategory_ids = []
            newscategory_ids.push({
              value: null,
              label: 'Semua Kategori',
            })
          for(let i=0; i < newscategories.length; i++){
            newscategory_ids.push({
              value: newscategories[i].id.toString(),
              label: newscategories[i].name,
            })
            for(let ic=0; ic < newscategories[i].subCategories.length; ic++){
              newscategory_ids.push({
              value: newscategories[i].subCategories[ic].id.toString(),
              label: newscategories[i].subCategories[ic].name,
            })
            }
          }
          this.initDatas.newscategory_ids = newscategory_ids
          setTimeout(() => {
              this.searchForm.newscategory_id = this.initDatas.newscategory_id
          })
    })
  }

  private generateWriterList():void{
    this.userService
        .getUserList()
        .then(users => {
          let writers = []
          writers.push({
            value: null,
            label: 'Semua User',
          })
          for(let i=0; i < users.length; i++){
            writers.push({
              value: users[i].id.toString(),
              label: users[i].first_name+' '+users[i].last_name,
            })
          }
          this.initDatas.writers = writers
          setTimeout(() => {
              this.searchForm.created_by = this.initDatas.created_by
          })
    })
  }
 
  private pageChanged(event: any): void {
    if( this.pageNews.current_page !== event.page )
      this.manipulateQueryParams([{ key : 'page', value : event.page}])
  }

  private searchData(): void {
    let searchForm = this.searchForm
    this.currentPage = 1
    let params = [{ key : 'page', value : 1 }]
    Object.keys(searchForm).map(function(k, i) {
      if( ( k === 'start_date' || k === 'end_date' ) && searchForm[k] !== null )
        params.push({ key : k, value : searchForm[k].formatted })
      else
        params.push({ key : k, value : searchForm[k] })
    })
    this.manipulateQueryParams(params)
  }
  
  private sortData(key, val): void {
    if( typeof this.queryParams['table_sort_by_order'] !== undefined )
      this.table_sort_by_order = this.queryParams['table_sort_by_order'] === 'asc' ? 'desc' : 'asc'
    this.table_sort_by_field = key
    this.manipulateQueryParams([
      { key : 'table_sort_by_field', value : this.table_sort_by_field },
      { key : 'table_sort_by_order', value : this.table_sort_by_order },
    ])
  }

  private manipulateQueryParams(paramArr){
    let params = this.queryParams
    
    Object.keys(paramArr).map(function(key, i) {
      if( paramArr[i].value !== null && paramArr[i].value !== '' )
        params[paramArr[i].key] = paramArr[i].value
      else
        delete params[paramArr[i].key]
    })
    let navigationExtras: NavigationExtras = {
      queryParams: params
    }
    this.router.navigate(['/news'], navigationExtras)
  }

  private deleteNews(id, title) {
    if(confirm('Hapus Data : '+title)) {
      this.newsService.delete(id).then(result => {
        this.pageNews.data = this.pageNews.data.filter(h => h.id !== result.id)
      })
    }
  }

  private getNews(){
    this.initDatas.errorLoad = false
    this.domService.loadingData = true
    this.newsService
      .index(this.queryParams)
      .then(result => {
        this.pageNews = result
        setTimeout(() => {
            this.currentPage = result.current_page
        })
        this.domService.loadingData = false
      })
      .catch((error) => {
        this.initDatas.errorLoad = true
        this.domService.loadingData = false
    })
  }

  private showDetailNews(id){
    this.newsModal.show()
    this.newsService
      .getNews(id)
      .then(result => {
        this.showNews = result
      })
      .catch((error) => {
        this.newsModal.hide()
        alert('Gagal memunculkan berita')
    })
  }

  ngOnInit(): void {
    this.generateWriterList()
    this.generateNewscategoryList()
  }

}
