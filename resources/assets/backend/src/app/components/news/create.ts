import { Component, OnInit } from '@angular/core'
import { FileUploader } from 'ng2-file-upload'
import { IMyDpOptions } from 'mydatepicker'
import { Configs } from '../../configs'

import { RoleObject } from '../../objects/role'
import { TagObject } from '../../objects/tag'
import { NewsObject } from '../../objects/news'
import { NewsImageObject } from '../../objects/newsImage'

import { DomService } from '../../services/dom'
import { LocalStorageService } from '../../services/localStorage'
import { TagService } from '../../services/tag'
import { NewsService } from '../../services/news'
import { NewscategoryService } from '../../services/newscategory'
import { ScrollToService } from 'ng2-scroll-to-el'

import { CKEDITOR } from '../ckeditor'

@Component({
  selector: 'create-news',
  templateUrl: '../../templates/news/create.html',
  styleUrls: [ '../../styles/news/create.css' ]
})

export class CreateNewsComponent implements OnInit {
	
  constructor(
    private domService: DomService, private localStorage: LocalStorageService, private newsService: NewsService, private newscategoryService: NewscategoryService, private tagService: TagService, private scrollService: ScrollToService) {
      setInterval(() => {
        if( !domService.savingDraft && !domService.savingData ){
          domService.savingDraft = true
          this.localStorage.save('new_news', this.news)
          domService.savingDraft = false
          this.news.news_type = this.newsService.getNewsType(this.news.content, this.news.images.length)
        }        
      }, 5000)
    }

  //private mask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/, ':', /[0-5]/, /[0-9]/]
  private mask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/]

  private initDatas = {
    successMessage: false,
    errorMessages: [],
    writingRole: null,
    contributor_ids : [],
    newscategory_ids : [],
    news_tags : [],
    uploader_errors : [],
  }

  private data_saved = this.localStorage.get('new_news')
  private news

  private myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'yyyy-mm-dd',
        //inline: false,
        //disableSince: {year: this.yyyy, month: this.mm, day: this.dd}
  };
  
  private uploader:FileUploader = new FileUploader({url: Configs.apiUrl+'files/news'});

  private requestTagItems = (q: string) => {
        return this.tagService.getTags(q)
  }

  private deleteNewsImage(newsImage: NewsImageObject): void {
    if (confirm('Hapus gambar ini ?')) {
      this.newsService
      .deleteNewsImage(newsImage.id, encodeURIComponent(newsImage.file_name), this.news.id)
      .then(() => {
        this.news.images = this.news.images.filter(h => h !== newsImage)
      });
    }
  }

  private buttonImageClass(isCover): string {
    if( isCover )
      return 'btn-primary';
    else 
      return 'btn-default';
  }

  private selectACover(image): void {
    for(let i=0; i < this.news.images.length; i++)
      this.news.images[i].cover = false
    image.cover = true
  }

  private onChangeContent(val: string) {
    this.news.content = val
  }

  private onChangeImageContent(val: string, model: any) {
    model.image_caption = val
  }

  private resetFormData(){
      this.news.contributor_id = null
      this.news.title = null
      this.news.show_writer_data = true
      this.news.subheader = null
      this.news.tags = []
      this.news.content = null
      this.news.parent_news_id = null
      this.news.source_link = null
      this.news.images = []
  }


  private initNews():void{
    let news_date, news_time;
    let today = new Date()
    if( today.getHours() > 17 && today.getHours() <= 24 )
      today.setDate(today.getDate() + 1)
    
    let dd = today.getDate()
    let mm = today.getMonth() + 1
    let yyyy = today.getFullYear()

    let HH = today.getHours() < 10 ? '0'+today.getHours() : today.getHours()
    let MM = today.getMinutes() < 10 ? '0'+today.getMinutes() : today.getMinutes()
    let ss = today.getSeconds() < 10 ? '0'+today.getSeconds() : today.getSeconds()

    if( today.getHours() > 17 && today.getHours() <= 24 ){
      news_date = { date: { year: yyyy, month: mm, day: dd }, formatted : yyyy +'-'+ ( mm < 10 ? '0'+mm.toString() : mm.toString() )+'-'+ ( dd < 10 ? '0'+dd.toString() : dd.toString() ) }
    }
    else if( this.data_saved !== null ){
      news_date = this.data_saved.news_date
    }
    else{
      news_date = { date: { year: yyyy, month: mm, day: dd }, formatted : yyyy +'-'+ ( mm < 10 ? '0'+mm.toString() : mm.toString() )+'-'+ ( dd < 10 ? '0'+dd.toString() : dd.toString() ) }
    }

    console.log(news_date)
    if( today.getHours() > 17 || today.getHours() < 4 ){
      news_time = '00:00'
    }
    else if( this.data_saved !== null ){
      news_time = this.data_saved.news_time
    }
    else{
      news_time = HH+':'+MM
    }

    this.news = {
      ignore_date : this.data_saved !== null ? this.data_saved.ignore_date : false,
      news_date: news_date,
      news_time: news_time,
      show_writer_data : this.data_saved !== null ? this.data_saved.show_writer_data : true,
      contributor_id : this.data_saved !== null ? this.data_saved.contributor_id : null,
      newscategory_id : this.data_saved !== null ? this.data_saved.newscategory_id : null,
      title : this.data_saved !== null ? this.data_saved.title : null,
      subheader : this.data_saved !== null ? this.data_saved.subheader : null,
      tags : this.data_saved !== null ? this.data_saved.tags : [],
      content: this.data_saved !== null ? this.data_saved.content : null,
      status: this.data_saved !== null ? this.data_saved.status : 1,
      news_type: this.data_saved !== null ? this.data_saved.news_type : null,
      parent_news_id: this.data_saved !== null ? this.data_saved.parent_news_id : null,
      source_link: this.data_saved !== null ? this.data_saved.source_link : null,
      id: null,
      created_at: '',
      updated_at: '',
      images: this.data_saved !== null ? this.data_saved.images : [],
      deleted_images: [],
      posted_at: '',
      hits: 0,
      created_by: 0
    }

    this.generateNewscategoryList()
  }

  private createNews(temporary:boolean=false){
    this.initDatas.errorMessages = []
    if( this.news.images.length === 1 && ( typeof this.news.images[0].image_caption === 'undefined' || this.news.images[0].image_caption === null ) ) {
      this.initDatas.errorMessages.push('Caption gambar tidak boleh kosong')
      return this.scrollService.scrollTo('.box-title')
    }
    if( this.news.images.length ){
      let hasCover = false
      for(let i=0; i < this.news.images.length; i++){
        if( typeof this.news.images[0].cover !== 'undefined' || this.news.images[0].cover )
          hasCover = true
      }
      if( !hasCover ){
        this.initDatas.errorMessages.push('Silahkan pilih satu cover gambar')
        return this.scrollService.scrollTo('.box-title')
      }
    }

    this.news.news_type = this.newsService.getNewsType(this.news.content, this.news.images.length)
    this.localStorage.save('new_news', this.news)
    this.domService.savingData = true
    let errorMessages = []
    this.newsService.create(this.localStorage.get('new_news'))
    .then(news => {
      this.localStorage.delete('new_news')
      this.resetFormData()
      this.initDatas.successMessage = true
      setTimeout(()=>{ this.initDatas.successMessage = false }, 4000)
      this.domService.savingData = false
      this.scrollService.scrollTo('.box-title')
    })
    .catch((error) => {
      this.domService.savingData = false
      if( error.status === 500){
        errorMessages.push(error.body.error)
      }
      else{
        Object.keys(error.body).map(function(key, index) {
          errorMessages.push(error.body[key])
        });
      }
        this.initDatas.errorMessages =  errorMessages
        this.scrollService.scrollTo('.box-title')
    })
  }


  private generateNewscategoryList():void{
    this.newscategoryService
        .getNewscategories()
        .then(newscategories => {
          let newscategory_ids = []
          for(let i=0; i < newscategories.length; i++){
            newscategory_ids.push({
              value: newscategories[i].id,
              label: newscategories[i].name,
            })
            for(let ic=0; ic < newscategories[i].subCategories.length; ic++){
              newscategory_ids.push({
              value: newscategories[i].subCategories[ic].id,
              label: newscategories[i].subCategories[ic].name,
            })
            }
          }
          this.initDatas.newscategory_ids = newscategory_ids;
          setTimeout(() => {
            if( this.data_saved !== null )
              this.news.newscategory_id = this.data_saved.newscategory_id
          });
    });
  }

  ngOnInit(): void {
    this.initNews()

	  this.initDatas.writingRole = '<p>#1: Menulis dengan jujur.<br /><br />#2: Perhatikan Tanda Baca.</p><br />'
    this.initDatas.contributor_ids = [
        {value: null, label: 'Pilih Salah Satu'},
        {value: '1', label: 'Art3mis'},
        {value: '2', label: 'Daito'},
        {value: '3', label: 'Parzival'},
        {value: '4', label: 'Shoto'}
    ]

    this.uploader.onAfterAddingFile = (item) => {item
      item.upload()
    }

    this.uploader.onBeforeUploadItem = (item) => {item
      item.withCredentials = false
    }
    
    this.uploader.onErrorItem = (item, response) => {
      this.initDatas.uploader_errors.push(JSON.parse(response).file)
      item.remove()
    }
    this.uploader.onSuccessItem = (item, response) => {
      this.initDatas.uploader_errors = []
      let image = JSON.parse(response).data
      if( !this.news.images.length )
        image.cover = true
      this.news.images.push(image)
      setTimeout(()=>{ this.uploader.removeFromQueue(item) }, 2000)      
    }
  }

}
