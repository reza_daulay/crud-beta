import { Component, Input } from '@angular/core';
 
@Component({
  selector: 'sidebar-wrapper',
  templateUrl: '../templates/sidebar-wrapper.html',
  styleUrls: ['../styles/sidebar-wrapper.css']
})
 
export class SidebarWrapperComponent {
	@Input() currentUrl: string;
}