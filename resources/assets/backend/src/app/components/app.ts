import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'

import { Configs } from '../configs'


import { DomService } from '../services/dom'
import { UserService } from '../services/user'

@Component({
  selector: 'my-app',
  templateUrl: '../templates/app.html',
  styleUrls: ['../styles/app.css', '../styles/app-responsive.css']
})

export class AppComponent implements OnInit {
	sidebarOpened:boolean = false
	currentUrl:string = ''
	
	constructor(
    private router: Router, private activatedRouter: ActivatedRoute, private domService: DomService, private userService: UserService) {
		router.events.subscribe((val) => {
			if(val instanceof NavigationEnd) {
				this.currentUrl = val.url
			}
		})
    }

    private configs = Configs
		
    toggleSidebar(): void {
		  this.sidebarOpened = !this.sidebarOpened ? true : false
    }

    private logOut(): void {
      this.userService.logOut().then().then(result => {
        window.location.reload()
      }).catch((error) => {
        window.location.reload()
      })      
    }
    ngOnInit(): void {
    }

}
