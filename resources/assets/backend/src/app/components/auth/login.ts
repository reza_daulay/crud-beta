import { Component, OnInit } from '@angular/core';

//import { LogoDesign } from '../objects/logoDesign';
//import { LogoDesignService } from '../services/logoDesign';

import { DomService } from '../../services/dom';

@Component({
  selector: 'login',
  templateUrl: '../templates/auth/login.html',
  //styleUrls: [ './styles/dashboard.css' ]
})

export class LoginComponent implements OnInit {
	
  constructor(
    private domService: DomService) { }

  ngOnInit(): void {
	  //
  }

}
