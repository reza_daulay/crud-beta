import { Component, ViewEncapsulation, Input } from '@angular/core'

import { RoleObject } from '../objects/role'

import { RoleService } from '../services/role'
 
@Component({
  selector: 'menu-accordion',
  templateUrl: '../templates/menu-accordion.html',
  //encapsulation: ViewEncapsulation.None,
  styleUrls: ['../styles/menu-accordion.css']
})
 
export class MenuAccordionComponent {
	constructor(
    private roleService: RoleService) {
  }

	@Input() currentUrl: string;
  private roles:RoleObject[]
  ngOnInit(): void {
    this.roleService.getRoles().subscribe(data => {
      this.roles = data
    })
  }
}