import { Component, OnInit } from '@angular/core';

//import { LogoDesign } from '../objects/logoDesign';
//import { LogoDesignService } from '../services/logoDesign';

import { DomService } from '../services/dom';

@Component({
  selector: 'dashboard',
  templateUrl: '../templates/dashboard.html',
  //styleUrls: [ './styles/dashboard.css' ]
})

export class DashboardComponent implements OnInit {
	
  constructor(
    private domService: DomService) { }

  ngOnInit(): void {
	  //
  }

}
