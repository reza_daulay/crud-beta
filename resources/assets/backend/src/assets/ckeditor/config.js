/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.language = 'id';
	config.height = '400px';
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

    //config.extraPlugins = 'youtube';
    config.removePlugins = 'iframe';
	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';
    

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	config.allowedContent = true; 

	//KCFINDER
    let base_url = 'http://localhost:8000/assets/backend';
	config.filebrowserBrowseUrl = base_url+'/filemanager/show';
	config.filebrowserUploadUrl = base_url+'/filemanager/upload?type=files';
    
    //config.stylesSet = 'my_styles:/bootstrap-table.css/';
    //config.contentsCss = '/assets/backend/css/bootsrap-autoloader/customs/css/bootstrap-table.css';
};
CKEDITOR.on( 'dialogDefinition', function( ev ) {
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if ( dialogName == 'table' ) {
        var info = dialogDefinition.getContents( 'info' );

        info.remove( 'txtWidth' );
        info.remove( 'txtBorder' );
        info.remove( 'txtCellSpace' );
        info.remove( 'txtCellPad' );
        
        dialogDefinition.removeContents('advanced');
    
        dialogDefinition.addContents( {
            id: 'advanced',
            label: 'Advanced',
            accessKey: 'A',
            elements: [
                {
                    type: 'select',
                    id: 'selClass',
                    label: 'Select the table class',
                    items: [ [ 'table' ], [ 'table table-striped'], [ 'table table-bordered'], [ 'table table-hover'], [ 'table table-condensed'] ],
                    'default': 'table',
                    setup: function(a) {
                        this.setValue(a.getAttribute("class") ||
                        "")
                    },
                    commit: function(a, d) {
                        this.getValue() ? d.setAttribute("class", this.getValue()) : d.removeAttribute("class")
                        d.setAttribute("width", "100%")
                        d.setAttribute("itemscope", "itemscope")
                        d.setAttribute("itemtype", "https://schema.org/Table")
                    }
                }
            ]
        });
        
    }
});
