<?php
return array(

    'image' => [
        'allow_extensions' => ['png', 'gif', 'jpg', 'jpeg'],
        'path' => 'assets/image/news/',
        'temp_path' => [
            'store' => 'assets/temp_user_image/news/store/',
            'update' => 'assets/temp_user_image/news/update/',
        ],
        'uploader' => [ 
            'size' => 150000,
        ],
        'large' => [ 
            'size' => 100000,
            'path' => 'big/',
            'width' => 715,
            'height' => 450,
        ],
        'thumbnail' => [
            'path' => 'small/',
            'width' => 280,
            'height' => 176,
        ],
    ]

);
