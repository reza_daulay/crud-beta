<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1', 'middleware' => ['auth'] ], function () {
	Route::get('roles', 'AuthController@roles');

	Route::resource('users', 'UserController', ['only' => [
		'index'
	]]);
	Route::resource('tags', 'TagController', ['only' => [
		'index'
	]]);
	Route::resource('newscategories', 'NewscategoryController', ['only' => [
		'index'
	]]);
	Route::resource('news', 'NewsController', ['only' => [
		'index', 'store', 'show', 'update', 'destroy'
	]]);

	Route::group(['namespace' => 'File', 'prefix' => 'files'], function () {
		Route::post('news/{id}', 'NewsController@update');
		Route::resource('news', 'NewsController', ['only' => [
					'index', 'store', 'update', 'destroy'
		]]);
	});
});
