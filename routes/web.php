<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::loginUsingId(29);

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout')->name('logout');

Route::group(array('middleware' => 'auth'), function(){
    Route::get('assets/backend/filemanager/show', 'Backend\FilemanagerLaravelController@getShow');
    Route::post('assets/backend/filemanager/upload', 'Backend\FilemanagerLaravelController@postUpload');
    Route::get('assets/backend/filemanager/connectors', 'Backend\FilemanagerLaravelController@getConnectors');
    Route::post('assets/backend/filemanager/connectors', 'Backend\FilemanagerLaravelController@postConnectors');
});
//http://localhost:8000/assets/backend/filemanager/show