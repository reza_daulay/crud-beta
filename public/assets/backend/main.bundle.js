webpackJsonp([1,4],{

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_dom__ = __webpack_require__(34);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//import { LogoDesign } from '../objects/logoDesign';
//import { LogoDesignService } from '../services/logoDesign';

var DashboardComponent = (function () {
    function DashboardComponent(domService) {
        this.domService = domService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        //
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'dashboard',
        template: __webpack_require__(345),
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_dom__["a" /* DomService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_dom__["a" /* DomService */]) === "function" && _a || Object])
], DashboardComponent);

var _a;
//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configs__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_dom__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_localStorage__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_tag__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_news__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_newscategory__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_scroll_to_el__ = __webpack_require__(102);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateNewsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var CreateNewsComponent = (function () {
    function CreateNewsComponent(domService, localStorage, newsService, newscategoryService, tagService, scrollService) {
        var _this = this;
        this.domService = domService;
        this.localStorage = localStorage;
        this.newsService = newsService;
        this.newscategoryService = newscategoryService;
        this.tagService = tagService;
        this.scrollService = scrollService;
        //private mask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/, ':', /[0-5]/, /[0-9]/]
        this.mask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/];
        this.initDatas = {
            successMessage: false,
            errorMessages: [],
            writingRole: null,
            contributor_ids: [],
            newscategory_ids: [],
            news_tags: [],
            uploader_errors: [],
        };
        this.data_saved = this.localStorage.get('new_news');
        this.myDatePickerOptions = {
            dateFormat: 'yyyy-mm-dd',
        };
        this.uploader = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_2__configs__["a" /* Configs */].apiUrl + 'files/news' });
        this.requestTagItems = function (q) {
            return _this.tagService.getTags(q);
        };
        setInterval(function () {
            if (!domService.savingDraft && !domService.savingData) {
                domService.savingDraft = true;
                _this.localStorage.save('new_news', _this.news);
                domService.savingDraft = false;
                _this.news.news_type = _this.newsService.getNewsType(_this.news.content, _this.news.images.length);
            }
        }, 5000);
    }
    CreateNewsComponent.prototype.deleteNewsImage = function (newsImage) {
        var _this = this;
        if (confirm('Hapus gambar ini ?')) {
            this.newsService
                .deleteNewsImage(newsImage.id, encodeURIComponent(newsImage.file_name), this.news.id)
                .then(function () {
                _this.news.images = _this.news.images.filter(function (h) { return h !== newsImage; });
            });
        }
    };
    CreateNewsComponent.prototype.buttonImageClass = function (isCover) {
        if (isCover)
            return 'btn-primary';
        else
            return 'btn-default';
    };
    CreateNewsComponent.prototype.selectACover = function (image) {
        for (var i = 0; i < this.news.images.length; i++)
            this.news.images[i].cover = false;
        image.cover = true;
    };
    CreateNewsComponent.prototype.onChangeContent = function (val) {
        this.news.content = val;
    };
    CreateNewsComponent.prototype.onChangeImageContent = function (val, model) {
        model.image_caption = val;
    };
    CreateNewsComponent.prototype.resetFormData = function () {
        this.news.contributor_id = null;
        this.news.title = null;
        this.news.show_writer_data = true;
        this.news.subheader = null;
        this.news.tags = [];
        this.news.content = null;
        this.news.parent_news_id = null;
        this.news.source_link = null;
        this.news.images = [];
    };
    CreateNewsComponent.prototype.initNews = function () {
        var news_date, news_time;
        var today = new Date();
        if (today.getHours() > 17 && today.getHours() <= 24)
            today.setDate(today.getDate() + 1);
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var HH = today.getHours() < 10 ? '0' + today.getHours() : today.getHours();
        var MM = today.getMinutes() < 10 ? '0' + today.getMinutes() : today.getMinutes();
        var ss = today.getSeconds() < 10 ? '0' + today.getSeconds() : today.getSeconds();
        if (today.getHours() > 17 && today.getHours() <= 24) {
            news_date = { date: { year: yyyy, month: mm, day: dd }, formatted: yyyy + '-' + (mm < 10 ? '0' + mm.toString() : mm.toString()) + '-' + (dd < 10 ? '0' + dd.toString() : dd.toString()) };
        }
        else if (this.data_saved !== null) {
            news_date = this.data_saved.news_date;
        }
        else {
            news_date = { date: { year: yyyy, month: mm, day: dd }, formatted: yyyy + '-' + (mm < 10 ? '0' + mm.toString() : mm.toString()) + '-' + (dd < 10 ? '0' + dd.toString() : dd.toString()) };
        }
        console.log(news_date);
        if (today.getHours() > 17 || today.getHours() < 4) {
            news_time = '00:00';
        }
        else if (this.data_saved !== null) {
            news_time = this.data_saved.news_time;
        }
        else {
            news_time = HH + ':' + MM;
        }
        this.news = {
            ignore_date: this.data_saved !== null ? this.data_saved.ignore_date : false,
            news_date: news_date,
            news_time: news_time,
            show_writer_data: this.data_saved !== null ? this.data_saved.show_writer_data : true,
            contributor_id: this.data_saved !== null ? this.data_saved.contributor_id : null,
            newscategory_id: this.data_saved !== null ? this.data_saved.newscategory_id : null,
            title: this.data_saved !== null ? this.data_saved.title : null,
            subheader: this.data_saved !== null ? this.data_saved.subheader : null,
            tags: this.data_saved !== null ? this.data_saved.tags : [],
            content: this.data_saved !== null ? this.data_saved.content : null,
            status: this.data_saved !== null ? this.data_saved.status : 1,
            news_type: this.data_saved !== null ? this.data_saved.news_type : null,
            parent_news_id: this.data_saved !== null ? this.data_saved.parent_news_id : null,
            source_link: this.data_saved !== null ? this.data_saved.source_link : null,
            id: null,
            created_at: '',
            updated_at: '',
            images: this.data_saved !== null ? this.data_saved.images : [],
            deleted_images: [],
            posted_at: '',
            hits: 0,
            created_by: 0
        };
        this.generateNewscategoryList();
    };
    CreateNewsComponent.prototype.createNews = function (temporary) {
        var _this = this;
        if (temporary === void 0) { temporary = false; }
        this.initDatas.errorMessages = [];
        if (this.news.images.length === 1 && (typeof this.news.images[0].image_caption === 'undefined' || this.news.images[0].image_caption === null)) {
            this.initDatas.errorMessages.push('Caption gambar tidak boleh kosong');
            return this.scrollService.scrollTo('.box-title');
        }
        if (this.news.images.length) {
            var hasCover = false;
            for (var i = 0; i < this.news.images.length; i++) {
                if (typeof this.news.images[0].cover !== 'undefined' || this.news.images[0].cover)
                    hasCover = true;
            }
            if (!hasCover) {
                this.initDatas.errorMessages.push('Silahkan pilih satu cover gambar');
                return this.scrollService.scrollTo('.box-title');
            }
        }
        this.news.news_type = this.newsService.getNewsType(this.news.content, this.news.images.length);
        this.localStorage.save('new_news', this.news);
        this.domService.savingData = true;
        var errorMessages = [];
        this.newsService.create(this.localStorage.get('new_news'))
            .then(function (news) {
            _this.localStorage.delete('new_news');
            _this.resetFormData();
            _this.initDatas.successMessage = true;
            setTimeout(function () { _this.initDatas.successMessage = false; }, 4000);
            _this.domService.savingData = false;
            _this.scrollService.scrollTo('.box-title');
        })
            .catch(function (error) {
            _this.domService.savingData = false;
            if (error.status === 500) {
                errorMessages.push(error.body.error);
            }
            else {
                Object.keys(error.body).map(function (key, index) {
                    errorMessages.push(error.body[key]);
                });
            }
            _this.initDatas.errorMessages = errorMessages;
            _this.scrollService.scrollTo('.box-title');
        });
    };
    CreateNewsComponent.prototype.generateNewscategoryList = function () {
        var _this = this;
        this.newscategoryService
            .getNewscategories()
            .then(function (newscategories) {
            var newscategory_ids = [];
            for (var i = 0; i < newscategories.length; i++) {
                newscategory_ids.push({
                    value: newscategories[i].id,
                    label: newscategories[i].name,
                });
                for (var ic = 0; ic < newscategories[i].subCategories.length; ic++) {
                    newscategory_ids.push({
                        value: newscategories[i].subCategories[ic].id,
                        label: newscategories[i].subCategories[ic].name,
                    });
                }
            }
            _this.initDatas.newscategory_ids = newscategory_ids;
            setTimeout(function () {
                if (_this.data_saved !== null)
                    _this.news.newscategory_id = _this.data_saved.newscategory_id;
            });
        });
    };
    CreateNewsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initNews();
        this.initDatas.writingRole = '<p>#1: Menulis dengan jujur.<br /><br />#2: Perhatikan Tanda Baca.</p><br />';
        this.initDatas.contributor_ids = [
            { value: null, label: 'Pilih Salah Satu' },
            { value: '1', label: 'Art3mis' },
            { value: '2', label: 'Daito' },
            { value: '3', label: 'Parzival' },
            { value: '4', label: 'Shoto' }
        ];
        this.uploader.onAfterAddingFile = function (item) {
            item;
            item.upload();
        };
        this.uploader.onBeforeUploadItem = function (item) {
            item;
            item.withCredentials = false;
        };
        this.uploader.onErrorItem = function (item, response) {
            _this.initDatas.uploader_errors.push(JSON.parse(response).file);
            item.remove();
        };
        this.uploader.onSuccessItem = function (item, response) {
            _this.initDatas.uploader_errors = [];
            var image = JSON.parse(response).data;
            if (!_this.news.images.length)
                image.cover = true;
            _this.news.images.push(image);
            setTimeout(function () { _this.uploader.removeFromQueue(item); }, 2000);
        };
    };
    return CreateNewsComponent;
}());
CreateNewsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'create-news',
        template: __webpack_require__(347),
        styles: [__webpack_require__(290)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_dom__["a" /* DomService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_dom__["a" /* DomService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_localStorage__["a" /* LocalStorageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_localStorage__["a" /* LocalStorageService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_6__services_news__["a" /* NewsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_news__["a" /* NewsService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_7__services_newscategory__["a" /* NewscategoryService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_newscategory__["a" /* NewscategoryService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_tag__["a" /* TagService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_tag__["a" /* TagService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_8_ng2_scroll_to_el__["b" /* ScrollToService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ng2_scroll_to_el__["b" /* ScrollToService */]) === "function" && _f || Object])
], CreateNewsComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=create.js.map

/***/ }),

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__configs__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_dom__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_localStorage__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_tag__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_news__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_newscategory__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_scroll_to_el__ = __webpack_require__(102);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditNewsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var EditNewsComponent = (function () {
    function EditNewsComponent(route, domService, localStorage, newsService, newscategoryService, tagService, scrollService) {
        var _this = this;
        this.route = route;
        this.domService = domService;
        this.localStorage = localStorage;
        this.newsService = newsService;
        this.newscategoryService = newscategoryService;
        this.tagService = tagService;
        this.scrollService = scrollService;
        this.mask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/, ':', /[0-5]/, /[0-9]/];
        this.initDatas = {
            newscategory_id: null,
            successMessage: false,
            errorLoad: false,
            news_id: null,
            errorMessages: [],
            writingRole: null,
            contributor_ids: [],
            newscategory_ids: [],
            news_tags: [],
            uploader_errors: [],
        };
        this.myDatePickerOptions = {
            dateFormat: 'yyyy-mm-dd',
        };
        this.uploader = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"]({ url: '', method: 'POST' });
        this.requestTagItems = function (q) {
            return _this.tagService.getTags(q);
        };
    }
    //private newsImages: NewsImageObject[];
    EditNewsComponent.prototype.getTemporaryNewsImages = function (news_id) {
        var _this = this;
        this.newsService
            .getTemporaryNewsImages(news_id)
            .then(function (newsImages) {
            var images = [];
            /*get uploaded news images*/
            for (var i = 0; i < newsImages.length; i++) {
                _this.news.images.push(newsImages[i]);
            }
        });
    };
    EditNewsComponent.prototype.deleteNewsImage = function (newsImage) {
        var _this = this;
        if (newsImage.id === null) {
            if (confirm('Hapus gambar ini ?')) {
                this.newsService
                    .deleteNewsImage(newsImage.id, encodeURIComponent(newsImage.file_name), this.news.id)
                    .then(function () {
                    _this.news.images = _this.news.images.filter(function (h) { return h !== newsImage; });
                });
            }
        }
        else {
            this.news.deleted_images.push(newsImage.id);
            this.news.images = this.news.images.filter(function (h) { return h !== newsImage; });
        }
        if (this.news.images.length === 1) {
            this.news.images[0].cover = true;
        }
    };
    EditNewsComponent.prototype.buttonImageClass = function (isCover) {
        if (isCover)
            return 'btn-primary';
        else
            return 'btn-default';
    };
    EditNewsComponent.prototype.selectACover = function (image) {
        for (var i = 0; i < this.news.images.length; i++)
            this.news.images[i].cover = false;
        image.cover = true;
    };
    EditNewsComponent.prototype.onChangeContent = function (val) {
        this.news.content = val;
    };
    EditNewsComponent.prototype.onChangeImageContent = function (val, model) {
        model.image_caption = val;
    };
    EditNewsComponent.prototype.updateNews = function () {
        var _this = this;
        this.initDatas.errorMessages = [];
        if (this.news.images.length === 1 && (typeof this.news.images[0].image_caption === 'undefined' || this.news.images[0].image_caption === null)) {
            this.initDatas.errorMessages.push('Caption gambar tidak boleh kosong');
            return this.scrollService.scrollTo('.box-title');
        }
        if (this.news.images.length) {
            var hasCover = false;
            for (var i = 0; i < this.news.images.length; i++) {
                if (typeof this.news.images[0].cover !== 'undefined' || this.news.images[0].cover)
                    hasCover = true;
            }
            if (!hasCover) {
                this.initDatas.errorMessages.push('Silahkan pilih satu cover gambar');
                return this.scrollService.scrollTo('.box-title');
            }
        }
        this.news.news_type = this.newsService.getNewsType(this.news.content, this.news.images.length);
        this.domService.savingData = true;
        var errorMessages = [];
        this.newsService.update(this.news)
            .then(function (news) {
            _this.initDatas.successMessage = true;
            setTimeout(function () { _this.initDatas.successMessage = false; }, 4000);
            _this.domService.savingData = false;
            _this.scrollService.scrollTo('.box-title');
            _this.news.deleted_images = [];
        })
            .catch(function (error) {
            if (error.status === 500) {
                errorMessages.push(error.body.error);
            }
            else {
                Object.keys(error.body).map(function (key, index) {
                    errorMessages.push(error.body[key]);
                });
            }
            _this.initDatas.errorMessages = errorMessages;
            _this.domService.savingData = false;
            _this.scrollService.scrollTo('.box-title');
        });
    };
    EditNewsComponent.prototype.generateNewscategoryList = function () {
        var _this = this;
        this.newscategoryService
            .getNewscategories()
            .then(function (newscategories) {
            var newscategory_ids = [];
            for (var i = 0; i < newscategories.length; i++) {
                newscategory_ids.push({
                    value: newscategories[i].id,
                    label: newscategories[i].name,
                });
                for (var ic = 0; ic < newscategories[i].subCategories.length; ic++) {
                    newscategory_ids.push({
                        value: newscategories[i].subCategories[ic].id,
                        label: newscategories[i].subCategories[ic].name,
                    });
                }
            }
            _this.initDatas.newscategory_ids = newscategory_ids;
            setTimeout(function () {
                _this.news.newscategory_id = _this.initDatas.newscategory_id;
            });
        });
    };
    EditNewsComponent.prototype.getNews = function () {
        var _this = this;
        this.initDatas.errorLoad = false;
        this.newsService.getNews(this.initDatas.news_id)
            .then(function (news) {
            _this.news = news;
            _this.news.deleted_images = [];
            _this.newsService.getNewsType(_this.news.content, _this.news.images.length);
            _this.news.newscategory_id = _this.news.newscategory_id.toString();
            _this.initDatas.newscategory_id = _this.news.newscategory_id.toString();
            _this.generateNewscategoryList();
            _this.getTemporaryNewsImages(_this.news.id);
            _this.uploader.options.url = __WEBPACK_IMPORTED_MODULE_3__configs__["a" /* Configs */].apiUrl + 'files/news/' + _this.news.id;
        })
            .catch(function (error) {
            _this.initDatas.errorLoad = true;
        });
    };
    EditNewsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.initDatas.news_id = params['id'];
            _this.getNews();
        });
        this.initDatas.writingRole = '<p>#1: Menulis dengan jujur.<br /><br />#2: Perhatikan Tanda Baca.</p><br />';
        this.initDatas.contributor_ids = [
            { value: null, label: 'Pilih Salah Satu' },
            { value: '1', label: 'Art3mis' },
            { value: '2', label: 'Daito' },
            { value: '3', label: 'Parzival' },
            { value: '4', label: 'Shoto' }
        ];
        this.uploader.onAfterAddingFile = function (item) {
            item;
            item.upload();
        };
        this.uploader.onBeforeUploadItem = function (item) {
            item.withCredentials = false;
        };
        this.uploader.onErrorItem = function (item, response) {
            _this.initDatas.uploader_errors.push(JSON.parse(response).file);
            item.remove();
        };
        this.uploader.onSuccessItem = function (item, response) {
            _this.initDatas.uploader_errors = [];
            var image = JSON.parse(response).data;
            if (!_this.news.images.length)
                image.isCover = true;
            _this.news.images.push(image);
            setTimeout(function () { _this.uploader.removeFromQueue(item); }, 2000);
        };
    };
    return EditNewsComponent;
}());
EditNewsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'edit-news',
        template: __webpack_require__(348),
        styles: [__webpack_require__(291)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_dom__["a" /* DomService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_dom__["a" /* DomService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__services_localStorage__["a" /* LocalStorageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_localStorage__["a" /* LocalStorageService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_7__services_news__["a" /* NewsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_news__["a" /* NewsService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_8__services_newscategory__["a" /* NewscategoryService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__services_newscategory__["a" /* NewscategoryService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__services_tag__["a" /* TagService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_tag__["a" /* TagService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_9_ng2_scroll_to_el__["b" /* ScrollToService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9_ng2_scroll_to_el__["b" /* ScrollToService */]) === "function" && _g || Object])
], EditNewsComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=edit.js.map

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__configs__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_dom__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_user__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_news__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_newscategory__ = __webpack_require__(54);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexNewsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var IndexNewsComponent = (function () {
    function IndexNewsComponent(router, activatedRoute, domService, userService, newsService, newscategoryService) {
        var _this = this;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.domService = domService;
        this.userService = userService;
        this.newsService = newsService;
        this.newscategoryService = newscategoryService;
        this.configs = __WEBPACK_IMPORTED_MODULE_3__configs__["a" /* Configs */];
        this.queryParams = [];
        this.table_sort_by_field = 'posted_at';
        this.table_sort_by_order = 'asc';
        this.searchForm = {
            start_date: null,
            end_date: null,
            newscategory_id: null,
            title: null,
            created_by: null,
        };
        this.myDatePickerOptions = {
            dateFormat: 'yyyy-mm-dd',
        };
        this.initDatas = {
            errorLoad: false,
            hasLoaded: false,
            newscategory_ids: [],
            newscategory_id: null,
            writers: [],
            created_by: null
        };
        this.currentPage = 1;
        this.router.events.subscribe(function (val) {
            if (val instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* NavigationEnd */])
                _this.getNews();
        });
        this.activatedRoute.queryParams.subscribe(function (params) {
            if (!_this.initDatas.hasLoaded) {
                if (Object.keys(params).length) {
                    for (var k in params) {
                        if (k === 'table_sort_by_field')
                            _this.table_sort_by_field = params[k];
                        else if (k === 'table_sort_by_order')
                            _this.table_sort_by_order = params[k];
                        else if (k === 'title')
                            _this.searchForm[k] = params[k];
                        else if (k === 'newscategory_id' || k === 'created_by')
                            _this.initDatas[k] = params[k];
                        else if (k === 'start_date' || k === 'end_date') {
                            var theDay = new Date(params[k]);
                            _this.searchForm[k] = { date: { year: theDay.getFullYear(), month: theDay.getMonth() + 1, day: theDay.getDate() }, formatted: params[k] };
                        }
                        _this.manipulateQueryParams([{ key: k, value: params[k] }]);
                    }
                }
                else {
                    _this.queryParams = [];
                }
            }
        });
    }
    IndexNewsComponent.prototype.generateNewscategoryList = function () {
        var _this = this;
        this.newscategoryService
            .getNewscategories()
            .then(function (newscategories) {
            var newscategory_ids = [];
            newscategory_ids.push({
                value: null,
                label: 'Semua Kategori',
            });
            for (var i = 0; i < newscategories.length; i++) {
                newscategory_ids.push({
                    value: newscategories[i].id.toString(),
                    label: newscategories[i].name,
                });
                for (var ic = 0; ic < newscategories[i].subCategories.length; ic++) {
                    newscategory_ids.push({
                        value: newscategories[i].subCategories[ic].id.toString(),
                        label: newscategories[i].subCategories[ic].name,
                    });
                }
            }
            _this.initDatas.newscategory_ids = newscategory_ids;
            setTimeout(function () {
                _this.searchForm.newscategory_id = _this.initDatas.newscategory_id;
            });
        });
    };
    IndexNewsComponent.prototype.generateWriterList = function () {
        var _this = this;
        this.userService
            .getUserList()
            .then(function (users) {
            var writers = [];
            writers.push({
                value: null,
                label: 'Semua User',
            });
            for (var i = 0; i < users.length; i++) {
                writers.push({
                    value: users[i].id.toString(),
                    label: users[i].first_name + ' ' + users[i].last_name,
                });
            }
            _this.initDatas.writers = writers;
            setTimeout(function () {
                _this.searchForm.created_by = _this.initDatas.created_by;
            });
        });
    };
    IndexNewsComponent.prototype.pageChanged = function (event) {
        if (this.pageNews.current_page !== event.page)
            this.manipulateQueryParams([{ key: 'page', value: event.page }]);
    };
    IndexNewsComponent.prototype.searchData = function () {
        var searchForm = this.searchForm;
        this.currentPage = 1;
        var params = [{ key: 'page', value: 1 }];
        Object.keys(searchForm).map(function (k, i) {
            if ((k === 'start_date' || k === 'end_date') && searchForm[k] !== null)
                params.push({ key: k, value: searchForm[k].formatted });
            else
                params.push({ key: k, value: searchForm[k] });
        });
        this.manipulateQueryParams(params);
    };
    IndexNewsComponent.prototype.sortData = function (key, val) {
        if (typeof this.queryParams['table_sort_by_order'] !== undefined)
            this.table_sort_by_order = this.queryParams['table_sort_by_order'] === 'asc' ? 'desc' : 'asc';
        this.table_sort_by_field = key;
        this.manipulateQueryParams([
            { key: 'table_sort_by_field', value: this.table_sort_by_field },
            { key: 'table_sort_by_order', value: this.table_sort_by_order },
        ]);
    };
    IndexNewsComponent.prototype.manipulateQueryParams = function (paramArr) {
        var params = this.queryParams;
        Object.keys(paramArr).map(function (key, i) {
            if (paramArr[i].value !== null && paramArr[i].value !== '')
                params[paramArr[i].key] = paramArr[i].value;
            else
                delete params[paramArr[i].key];
        });
        var navigationExtras = {
            queryParams: params
        };
        this.router.navigate(['/news'], navigationExtras);
    };
    IndexNewsComponent.prototype.deleteNews = function (id, title) {
        var _this = this;
        if (confirm('Hapus Data : ' + title)) {
            this.newsService.delete(id).then(function (result) {
                _this.pageNews.data = _this.pageNews.data.filter(function (h) { return h.id !== result.id; });
            });
        }
    };
    IndexNewsComponent.prototype.getNews = function () {
        var _this = this;
        this.initDatas.errorLoad = false;
        this.domService.loadingData = true;
        this.newsService
            .index(this.queryParams)
            .then(function (result) {
            _this.pageNews = result;
            setTimeout(function () {
                _this.currentPage = result.current_page;
            });
            _this.domService.loadingData = false;
        })
            .catch(function (error) {
            _this.initDatas.errorLoad = true;
            _this.domService.loadingData = false;
        });
    };
    IndexNewsComponent.prototype.showDetailNews = function (id) {
        var _this = this;
        this.newsModal.show();
        this.newsService
            .getNews(id)
            .then(function (result) {
            _this.showNews = result;
        })
            .catch(function (error) {
            _this.newsModal.hide();
            alert('Gagal memunculkan berita');
        });
    };
    IndexNewsComponent.prototype.ngOnInit = function () {
        this.generateWriterList();
        this.generateNewscategoryList();
    };
    return IndexNewsComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('newsModal'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["b" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["b" /* ModalDirective */]) === "function" && _a || Object)
], IndexNewsComponent.prototype, "newsModal", void 0);
IndexNewsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'index-news',
        template: __webpack_require__(349),
        styles: [__webpack_require__(292)]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_dom__["a" /* DomService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_dom__["a" /* DomService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_user__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_user__["a" /* UserService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__services_news__["a" /* NewsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_news__["a" /* NewsService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_7__services_newscategory__["a" /* NewscategoryService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_newscategory__["a" /* NewscategoryService */]) === "function" && _g || Object])
], IndexNewsComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=index.js.map

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_role__ = __webpack_require__(76);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuardService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuardService = (function () {
    function AuthGuardService(router, roleService) {
        this.router = router;
        this.roleService = roleService;
    }
    AuthGuardService.prototype.canActivate = function (route, state) {
        var url = state.url;
        return this.checkRole(route.data.name);
    };
    AuthGuardService.prototype.checkRole = function (routeName) {
        var _this = this;
        this.roleService.getRoles().subscribe(function (roles) {
            if (_this.roleService.hasRole(roles, routeName)) {
                return true;
            }
            else {
                _this.router.navigate(['/']);
                window.location.reload();
            }
        });
        return true;
    };
    return AuthGuardService;
}());
AuthGuardService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_role__["a" /* RoleService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_role__["a" /* RoleService */]) === "function" && _b || Object])
], AuthGuardService);

var _a, _b;
//# sourceMappingURL=authGuard.js.map

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Configs; });
var Configs = {
    'baseDomain': document.getElementsByTagName('base-domain')[0].getAttribute('content'),
    'baseAsset': document.getElementsByTagName('base-asset')[0].getAttribute('content'),
    'apiUrl': document.getElementsByTagName('api-url')[0].getAttribute('content')
};
//# sourceMappingURL=configs.js.map

/***/ }),

/***/ 214:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 214;


/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(230);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_authGuard__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_dashboard__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_news_index__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_news_create__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_news_edit__ = __webpack_require__(126);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



//import { LoginComponent }   from './components/auth/login




var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_3__components_dashboard__["a" /* DashboardComponent */], pathMatch: 'full', data: { 'name': 'index' } },
    { path: 'news', component: __WEBPACK_IMPORTED_MODULE_4__components_news_index__["a" /* IndexNewsComponent */], data: { 'name': 'news.view' }, canActivate: [__WEBPACK_IMPORTED_MODULE_2__services_authGuard__["a" /* AuthGuardService */]] },
    { path: 'news/create', component: __WEBPACK_IMPORTED_MODULE_5__components_news_create__["a" /* CreateNewsComponent */], data: { 'name': 'news.add' }, canActivate: [__WEBPACK_IMPORTED_MODULE_2__services_authGuard__["a" /* AuthGuardService */]], },
    { path: 'news/:id/edit', component: __WEBPACK_IMPORTED_MODULE_6__components_news_edit__["a" /* EditNewsComponent */], data: { 'name': 'news.edit' }, canActivate: [__WEBPACK_IMPORTED_MODULE_2__services_authGuard__["a" /* AuthGuardService */]] },
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forRoot(routes, { useHash: true })],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
    })
], AppRoutingModule);

//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_routing_module__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_dropdown__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap_accordion__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ngx_bootstrap_typeahead__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ngx_bootstrap_pagination__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_bootstrap_carousel__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ngui_datetime_picker__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ngui_datetime_picker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12__ngui_datetime_picker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angular2_text_mask__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angular2_text_mask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_angular2_text_mask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ng2_ui_switch__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_ng_select__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_ng_select___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_ng_select__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_ng2_tag_input__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_ng2_tag_input___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_ng2_tag_input__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_ng2_file_upload__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_ng2_scroll_to_el__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_ngx_mydatepicker__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_ckeditor__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_menu_accordion__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_sidebar_wrapper__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_app__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_dashboard__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_news_index__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__components_news_create__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__components_news_edit__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__services_dom__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__services_authGuard__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__services_localStorage__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__services_user__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__services_role__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__services_tag__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__services_news__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__services_newscategory__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__directives_match_height__ = __webpack_require__(229);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















//import { MyDatePickerModule } from 'mydatepicker'









//import { LoginComponent }   from './components/auth/login'









__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_5__app_routing_module__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap_accordion__["a" /* AccordionModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__["a" /* ModalModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_9_ngx_bootstrap_typeahead__["a" /* TypeaheadModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_18_ng2_scroll_to_el__["a" /* ScrollToModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_10_ngx_bootstrap_pagination__["a" /* PaginationModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_11_ngx_bootstrap_carousel__["a" /* CarouselModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_12__ngui_datetime_picker__["NguiDatetimePickerModule"],
            __WEBPACK_IMPORTED_MODULE_13_angular2_text_mask__["TextMaskModule"],
            __WEBPACK_IMPORTED_MODULE_14_ng2_ui_switch__["a" /* UiSwitchModule */],
            __WEBPACK_IMPORTED_MODULE_15_ng_select__["SelectModule"],
            __WEBPACK_IMPORTED_MODULE_16_ng2_tag_input__["TagInputModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_17_ng2_file_upload__["FileUploadModule"],
            //MyDatePickerModule,
            __WEBPACK_IMPORTED_MODULE_19_ngx_mydatepicker__["NgxMyDatePickerModule"],
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_20__components_ckeditor__["a" /* CKEDITOR */],
            __WEBPACK_IMPORTED_MODULE_23__components_app__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_21__components_menu_accordion__["a" /* MenuAccordionComponent */],
            __WEBPACK_IMPORTED_MODULE_22__components_sidebar_wrapper__["a" /* SidebarWrapperComponent */],
            __WEBPACK_IMPORTED_MODULE_36__directives_match_height__["a" /* MatchHeightDirective */],
            __WEBPACK_IMPORTED_MODULE_24__components_dashboard__["a" /* DashboardComponent */],
            __WEBPACK_IMPORTED_MODULE_25__components_news_index__["a" /* IndexNewsComponent */],
            __WEBPACK_IMPORTED_MODULE_26__components_news_create__["a" /* CreateNewsComponent */],
            __WEBPACK_IMPORTED_MODULE_27__components_news_edit__["a" /* EditNewsComponent */],
        ],
        providers: [
            /*ErrorNotifierService,
            {
              provide: Http,
              useFactory: (backend: XHRBackend, defaultOptions: RequestOptions, errorNotifier: ErrorNotifierService) => {
                return new CustomHttpService(backend, defaultOptions, errorNotifier);
              },
              deps: [ XHRBackend, RequestOptions, ErrorNotifierService ]
            },
            {
              provide: RequestOptions, useClass: AppRequestOptions
            },*/
            __WEBPACK_IMPORTED_MODULE_28__services_dom__["a" /* DomService */],
            __WEBPACK_IMPORTED_MODULE_29__services_authGuard__["a" /* AuthGuardService */],
            __WEBPACK_IMPORTED_MODULE_30__services_localStorage__["a" /* LocalStorageService */],
            __WEBPACK_IMPORTED_MODULE_31__services_user__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_32__services_role__["a" /* RoleService */],
            __WEBPACK_IMPORTED_MODULE_33__services_tag__["a" /* TagService */],
            __WEBPACK_IMPORTED_MODULE_34__services_news__["a" /* NewsService */],
            __WEBPACK_IMPORTED_MODULE_35__services_newscategory__["a" /* NewscategoryService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_23__components_app__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configs__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_dom__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_user__ = __webpack_require__(78);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = (function () {
    function AppComponent(router, activatedRouter, domService, userService) {
        var _this = this;
        this.router = router;
        this.activatedRouter = activatedRouter;
        this.domService = domService;
        this.userService = userService;
        this.sidebarOpened = false;
        this.currentUrl = '';
        this.configs = __WEBPACK_IMPORTED_MODULE_2__configs__["a" /* Configs */];
        router.events.subscribe(function (val) {
            if (val instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* NavigationEnd */]) {
                _this.currentUrl = val.url;
            }
        });
    }
    AppComponent.prototype.toggleSidebar = function () {
        this.sidebarOpened = !this.sidebarOpened ? true : false;
    };
    AppComponent.prototype.logOut = function () {
        this.userService.logOut().then().then(function (result) {
            window.location.reload();
        }).catch(function (error) {
            window.location.reload();
        });
    };
    AppComponent.prototype.ngOnInit = function () {
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'my-app',
        template: __webpack_require__(344),
        styles: [__webpack_require__(288), __webpack_require__(287)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_dom__["a" /* DomService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_dom__["a" /* DomService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_user__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_user__["a" /* UserService */]) === "function" && _d || Object])
], AppComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=app.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CKEDITOR; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CKEDITOR = (function () {
    function CKEDITOR() {
        this.rows = '10';
        this.cols = '10';
        this.type = 'default';
        this.value = null;
        this.onChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.editor = null;
        this.ckeditorReady = false;
    }
    CKEDITOR.prototype.ngAfterViewInit = function () {
        if (this.type === 'simple') {
            this.editor = window['CKEDITOR']['replace'](this.targetId, {
                height: '500px',
                toolbarGroups: [
                    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
                    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
                    { name: 'insert' },
                    { name: 'styles' },
                ],
                removeButtons: 'Flash,Smiley,Anchor,Iframe,Subscript,Superscript,CreateDiv'
            });
        }
        else if (this.type === 'small') {
            this.editor = window['CKEDITOR']['replace'](this.targetId, {
                height: '120px',
                toolbarGroups: [
                    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
                    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
                ],
                removeButtons: 'Flash,Image,Smiley,Anchor,Iframe,Subscript,Superscript,CreateDiv'
            });
        }
        else {
            this.editor = window['CKEDITOR']['replace'](this.targetId);
        }
        var base = this;
        this.editor.on('change', function () {
            base.onChange.emit(base.editor.getData());
        });
        this.ckeditorReady = true;
    };
    CKEDITOR.prototype.ngOnChanges = function (changes) {
        var tvalue = changes.value;
        //console.log('prev value: ', tvalue.previousValue);
        //console.log('got value: ', tvalue.currentValue);
        if (this.ckeditorReady && tvalue.currentValue === null) {
            console.log('ckeditorReady');
            this.editor.setData('');
        }
    };
    return CKEDITOR;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('id'),
    __metadata("design:type", String)
], CKEDITOR.prototype, "targetId", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('model'),
    __metadata("design:type", Object)
], CKEDITOR.prototype, "targetModel", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('row'),
    __metadata("design:type", String)
], CKEDITOR.prototype, "rows", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('cols'),
    __metadata("design:type", String)
], CKEDITOR.prototype, "cols", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('type'),
    __metadata("design:type", String)
], CKEDITOR.prototype, "type", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('value'),
    __metadata("design:type", String)
], CKEDITOR.prototype, "value", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], CKEDITOR.prototype, "onChange", void 0);
CKEDITOR = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'CKEDITOR',
        template: "<textarea name=\"{{ targetId }}\" id=\"{{ targetId }}\" rows=\"{{ rows }}\" cols=\"{{ cols }}\">{{ value }}</textarea>"
    })
], CKEDITOR);

//# sourceMappingURL=ckeditor.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_role__ = __webpack_require__(76);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuAccordionComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuAccordionComponent = (function () {
    function MenuAccordionComponent(roleService) {
        this.roleService = roleService;
    }
    MenuAccordionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.roleService.getRoles().subscribe(function (data) {
            _this.roles = data;
        });
    };
    return MenuAccordionComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], MenuAccordionComponent.prototype, "currentUrl", void 0);
MenuAccordionComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'menu-accordion',
        template: __webpack_require__(346),
        //encapsulation: ViewEncapsulation.None,
        styles: [__webpack_require__(289)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_role__["a" /* RoleService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_role__["a" /* RoleService */]) === "function" && _a || Object])
], MenuAccordionComponent);

var _a;
//# sourceMappingURL=menu-accordion.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarWrapperComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarWrapperComponent = (function () {
    function SidebarWrapperComponent() {
    }
    return SidebarWrapperComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], SidebarWrapperComponent.prototype, "currentUrl", void 0);
SidebarWrapperComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'sidebar-wrapper',
        template: __webpack_require__(350),
        styles: [__webpack_require__(293)]
    })
], SidebarWrapperComponent);

//# sourceMappingURL=sidebar-wrapper.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MatchHeightDirective; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MatchHeightDirective = (function () {
    function MatchHeightDirective(el) {
        this.el = el;
    }
    MatchHeightDirective.prototype.ngAfterViewChecked = function () {
        // call our matchHeight function here later
        this.matchHeight(this.el.nativeElement, this.myMatchHeight);
    };
    MatchHeightDirective.prototype.onResize = function () {
        // call our matchHeight function here later
        this.matchHeight(this.el.nativeElement, this.myMatchHeight);
    };
    MatchHeightDirective.prototype.matchHeight = function (parent, className) {
        // match height logic here
        if (!parent)
            return;
        var children = parent.getElementsByClassName(className);
        if (!children)
            return;
        // reset all children height
        Array.from(children).forEach(function (x) {
            x.style.height = 'initial';
        });
        // gather all height
        var itemHeights = Array.from(children)
            .map(function (x) { return x.getBoundingClientRect().height; });
        // find max height
        var maxHeight = itemHeights.reduce(function (prev, curr) {
            return curr > prev ? curr : prev;
        }, 0);
        // apply max height
        Array.from(children)
            .forEach(function (x) { return x.style.height = maxHeight + "px"; });
    };
    return MatchHeightDirective;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], MatchHeightDirective.prototype, "myMatchHeight", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:resize'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], MatchHeightDirective.prototype, "onResize", null);
MatchHeightDirective = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[myMatchHeight]'
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object])
], MatchHeightDirective);

var _a;
//# sourceMappingURL=match-height.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 287:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(21)(false);
// imports


// module
exports.push([module.i, "\r\n@media(min-width:768px) {\r\n    #wrapper {\r\n        padding-left: 0;\r\n    }\r\n\r\n    #wrapper.toggled {\r\n        padding-left: 250px;\r\n    }\r\n\r\n    #wrapper.toggled >>> #sidebar-wrapper {\r\n        width: 250px;\r\n    }\r\n\r\n    #page-content-wrapper {\r\n        padding: 20px;\r\n        position: relative;\r\n    }\r\n\r\n    #wrapper.toggled #page-content-wrapper {\r\n        position: relative;\r\n        margin-right: 0;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 288:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(21)(false);
// imports


// module
exports.push([module.i, "/*!\r\n * Start Bootstrap - Simple Sidebar (http://startbootstrap.com/)\r\n * Copyright 2013-2016 Start Bootstrap\r\n * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)\r\n */\r\n\r\nbody {\r\n\toverflow-x: hidden;\r\n}\r\n\r\n.pointer{\r\n\tcursor: pointer;\r\n}\r\n\r\n/* Toggle Styles */\r\n#wrapper {\r\n    padding-left: 0;\r\n    transition: all 0.5s ease;\r\n}\r\n\r\n#wrapper.toggled {\r\n    padding-left: 250px;\r\n}\r\n\r\n\r\n#page-content-wrapper {\r\n    width: 100%;\r\n    position: absolute;\r\n    padding: 15px;\r\n}\r\n\r\n#wrapper.toggled #page-content-wrapper {\r\n    position: absolute;\r\n    margin-right: -250px;\r\n}\r\n#navbar{\r\n\tpadding-left:0;\r\n}\r\n.menu-bar i{\r\n\tfont-size: 20px;\r\n\tvertical-align: bottom;\r\n}\r\n.menu-bar span{\r\n    font-size: 15px;\r\n\tpadding-left: 3px;\r\n}\r\n\r\n\r\n#wrapper.toggled >>> #sidebar-wrapper {\r\n    width: 250px;\r\n}\r\n/* accordion */\r\n#wrapper >>> .panel-group .panel {\r\n    border-radius: 0;\r\n\tborder: 0;\r\n\tborder-bottom: 1px solid #ddd;\r\n\tbackground-color: #000000;\r\n}\r\n\r\n#wrapper >>> .first > .panel-default {\r\n\tborder-top: 1px solid #ddd;\r\n}\r\n\r\n#wrapper >>> .panel-default>.panel-heading {\r\n    border-radius: 0;\r\n\tbackground-color: #000000;\r\n\tcolor: #fff;\r\n\tpadding: 10px 5px;\r\n}\r\n\r\n#wrapper >>> .panel-default>.panel-heading .fa.arrow{\r\n\tfloat: right;\r\n    padding-right: 5px;\r\n}\r\n\r\n#wrapper >>> .panel-default>.panel-collapse>.panel-body a{\r\n    width: 100%;\r\n    display: block;\r\n    margin-bottom: 5px;\r\n    color: #fff;\r\n}\r\n\r\n#wrapper >>> .panel-default>.panel-collapse>.panel-body a:hover,\r\n#wrapper >>> .panel-default>.panel-collapse>.panel-body a:active,\r\n#wrapper >>> .panel-default>.panel-collapse>.panel-body a:visited,\r\n#wrapper >>> .panel-default>.panel-collapse>.panel-body a:focus{\r\n\ttext-decoration:none;\r\n\tcolor: #f5f5f5;\r\n}\r\n\r\n#saving-draft{\r\n    position: fixed;\r\n    top: 0px;\r\n    /* center the element */\r\n    right: 0;\r\n    left: 0;\r\n    margin-right: auto;\r\n    margin-left: auto;\r\n    /* give it dimensions */\r\n    min-height: 12px;\r\n    width: 90%;\r\n    text-align: center;\r\n}\r\n#saving-draft >>> p{\r\n    padding: 10px;\r\n    background: #000;\r\n    color: #fff;\r\n    display: inline-block;\r\n    font-size: 16px;\r\n}\r\n#saving-data-overlay{\r\n    position: fixed;\r\n    top: 0px;\r\n    bottom: 0px;\r\n    right: 0;\r\n    left: 0;\r\n    background: transparent;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 289:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(21)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 290:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(21)(false);
// imports


// module
exports.push([module.i, ".list-images{\r\n    margin-top: 25px;\r\n}\r\n\r\n.list-images >>> li{\r\n    margin-bottom: 25px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 291:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(21)(false);
// imports


// module
exports.push([module.i, ".list-images{\r\n    margin-top: 25px;\r\n}\r\n\r\n.list-images >>> li{\r\n    margin-bottom: 25px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 292:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(21)(false);
// imports


// module
exports.push([module.i, ".table-index-search{\r\n    margin-bottom: 20px;\r\n}\r\n.table-index-search >>> .ng-select{\r\n    width:200px;\r\n}\r\n.table-index-data >>> th >>> .fa-chevron-down,\r\n.table-index-data >>> th >>> .fa-chevron-up{\r\n    float: right;\r\n}\r\n.table-index-data >>> th.sortable{\r\n    cursor: pointer;\r\n}\r\n.table-index-data >>> tr >>> td >>> .img-data{\r\n    max-width: 100px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 293:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(21)(false);
// imports


// module
exports.push([module.i, "#sidebar-wrapper {\r\n    z-index: 1000;\r\n    position: fixed;\r\n    left: 250px;\r\n    width: 0;\r\n    height: 100%;\r\n    margin-left: -250px;\r\n    overflow-y: auto;\r\n\toverflow-x: hidden;\r\n    background: #000;\r\n    transition: all 0.5s ease;\r\n}\r\n\r\n#sidebar-wrapper .panel-group .panel{\r\n\tborder-radius: 0;\r\n\tbackground: none;\r\n}\r\n\r\n/* Sidebar Styles */\r\n\r\n.sidebar-nav {\r\n    position: absolute;\r\n    top: 0;\r\n    width: 250px;\r\n    margin: 0;\r\n    padding: 0;\r\n    list-style: none;\r\n}\r\n\r\n.sidebar-nav li {\r\n    text-indent: 20px;\r\n    line-height: 40px;\r\n}\r\n\r\n.sidebar-nav li a {\r\n    display: block;\r\n    text-decoration: none;\r\n    color: #999999;\r\n}\r\n\r\n.sidebar-nav li a:hover {\r\n    text-decoration: none;\r\n    color: #fff;\r\n    background: rgba(255,255,255,0.2);\r\n}\r\n\r\n.sidebar-nav li a:active,\r\n.sidebar-nav li a:focus {\r\n    text-decoration: none;\r\n}\r\n\r\n.sidebar-nav > .sidebar-brand {\r\n    height: 65px;\r\n    font-size: 18px;\r\n    line-height: 60px;\r\n}\r\n\r\n.sidebar-nav > .sidebar-brand a {\r\n    color: #999999;\r\n}\r\n\r\n.sidebar-nav > .sidebar-brand a:hover {\r\n    color: #fff;\r\n    background: none;\r\n}\r\n\r\n\r\n@media(min-width:768px) {\r\n    #sidebar-wrapper {\r\n        width: 0;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DomService; });
var DomService = (function () {
    function DomService() {
        this.savingDraft = false;
        this.savingData = false;
        this.loadingData = false;
        this.validationError = null;
    }
    return DomService;
}());

//# sourceMappingURL=dom.js.map

/***/ }),

/***/ 344:
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\" [class.toggled]=\"sidebarOpened\" >\n\t<sidebar-wrapper [currentUrl]=\"currentUrl\"></sidebar-wrapper>\n\t\t\n\t<nav id=\"main-navbar\" class=\"navbar navbar-default\">\n\t\t<div class=\"container-fluid\">\n\t\t  <div class=\"navbar-header\">\n\t\t\t<button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\n\t\t\t  <span class=\"sr-only\">Toggle navigation</span>\n\t\t\t  <span class=\"icon-bar\"></span>\n\t\t\t  <span class=\"icon-bar\"></span>\n\t\t\t  <span class=\"icon-bar\"></span>\n\t\t\t</button>\n\t\t  </div>\n\t\t  <div id=\"navbar\" class=\"navbar-collapse collapse\">\n\t\t\t<ul class=\"nav navbar-nav\">\n\t\t\t  <li><a class=\"pointer menu-bar\" (click)=\"toggleSidebar()\" ><i class=\"fa fa-bars\" aria-hidden=\"true\"></i> <span>Menu</span></a></li>\n\t\t\t</ul>\n\t\t\t<ul class=\"nav navbar-nav navbar-right\">\n\t\t\t  <li class=\"dropdown\" dropdown (isOpenChange)=\"toggled($event)\">\n\t\t\t\t  <a href dropdownToggle (click)=\"false\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Akun<span class=\"caret\"></span></a>\n\t\t\t\t\t<ul *dropdownMenu class=\"dropdown-menu\">\n\t\t\t\t\t  <!--<li><a href=\"#\">Setting</a></li>-->\n\t\t\t\t\t  <!--<li><a href=\"#\">Statistik Pribadi</a></li>-->\n\t\t\t\t\t  <!--<li role=\"separator\" class=\"divider\"></li>-->\n\t\t\t\t\t  <li><a (click)=\"logOut()\">Keluar</a></li>\n\t\t\t\t\t</ul>\n\t\t\t  </li>\n\t\t\t</ul>\n\t\t  </div>\n\t\t</div>\n\t</nav>\n\n\t<div id=\"page-content-wrapper\">\n\t\t<div class=\"container-fluid\">\n\t\t\t<router-outlet></router-outlet>\n\t\t</div>\n\t</div>\n\n</div>\n\n<div id=\"saving-draft\" *ngIf=\"domService.savingDraft\">\n\t<p><i class=\"fa fa-spinner fa-spin fa-fw\"></i> Sedang Menyimpan Draft</p>\n</div>\n\n<div id=\"saving-data-overlay\" *ngIf=\"domService.savingData\"></div>"

/***/ }),

/***/ 345:
/***/ (function(module, exports) {

module.exports = "Dashboard"

/***/ }),

/***/ 346:
/***/ (function(module, exports) {

module.exports = "<li *ngIf=\"roles\">\r\n\t<accordion>\r\n\t  <accordion-group [ngClass]=\"'first'\" #group1 [isOpen]=\"currentUrl === '/news' || currentUrl === '/news/create'\">\r\n\t\t<div accordion-heading>\r\n\t\t\t<i class=\"icon-left fa fa-newspaper-o\" aria-hidden=\"true\"></i>\r\n\t\t\t<span>Berita</span>\r\n\t\t\t<i class=\"fa arrow icon-right\" [ngClass]=\"{'fa-caret-down': group1?.isOpen, 'fa-caret-right': !group1?.isOpen}\" aria-hidden=\"true\"></i>\r\n\t\t</div>\r\n\t\t<a routerLink=\"/news/create\" routerLinkActive=\"active\">Tambah Baru</a>\r\n\t\t<a routerLink=\"/news\" routerLinkActive=\"active\" *ngIf=\"roleService.hasRole(roles, 'news.view')\">Daftar Berita</a>\r\n\t  </accordion-group>\r\n\t</accordion>\r\n</li>"

/***/ }),

/***/ 347:
/***/ (function(module, exports) {

module.exports = "<ol class=\"breadcrumb\">\n\t<li><a routerLink=\"/\">Home</a></li>\n\t<li><a routerLink=\"/news\">Daftar Berita</a></li>\n\t<li class=\"active\">Tambah Berita Baru</li>\n</ol>\n\n<div class=\"row\" *ngIf=\"news\">\n    <form method=\"POST\" class=\"operator-form\" action=\"\" accept-charset=\"UTF-8\" role=\"form\">\n        <div class=\"col-md-8 col-md-offset-2\">\n            <h3 class=\"box-title\">Data Isi Berita</h3>\n            <div class=\"alert alert-warning\">\n                <strong>Untuk Operator Malam</strong> status berita saat ini otomatis tunda dan waktu berita 00:00 pada tanggal keesokan harinya\n            </div>\n            <div *ngIf=\"initDatas.errorMessages.length\" class=\"alert alert-danger\">\n                <strong>Error!</strong>\n                <ul>\n                    <li *ngFor=\"let errorMessage of initDatas.errorMessages\">\n                        {{ errorMessage }}\n                    </li>\n                </ul>\n            </div>\n            <div *ngIf=\"initDatas.successMessage\" class=\"alert alert-success\">\n                <strong>Sukses!</strong> Berita berhasil disimpan\n            </div>\n\n            <!--button id=\"aturan-penulisan\" type=\"button\" class=\"btn btn-primary\" (click)=\"writingRoleModal.show()\">Aturan Penulisan</button-->\n            <div class=\"form-group\">\n                <label>Abaikan Kolom Isian Tanggal Berita:</label>\n                <div class=\"input-group\">\n                    <label id=\"ignore_date\" (click)=\"news.ignore_date !== 1 ? news.ignore_date = 1 : news.ignore_date = 0\" >\n                        <input type=\"checkbox\" [checked]=\"news.ignore_date === 1\" class=\"simple\"  checked   /> Jika ini di centang maka saat input berita , jam berita otomatis mengikuti jam terbaru .\n                    </label>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label>Tanggal Berita <strong class=\"required\">*</strong></label>\n                <div class=\"input-group col-md-3\">\n                    <input class=\"form-control\" placeholder=\"Dari Tanggal\" ngx-mydatepicker name=\"news_date\"\n                            [(ngModel)]=\"news.news_date\" [options]=\"myDatePickerOptions\" #dp1=\"ngx-mydatepicker\"/>\n                    <span class=\"input-group-btn\">\n                        <button type=\"button\" class=\"btn btn-default\" (click)=\"dp1.clearDate()\">\n                            <i class=\"glyphicon glyphicon-remove\"></i>\n                        </button>\n                        <button type=\"button\" class=\"btn btn-default\" (click)=\"dp1.toggleCalendar()\">\n                            <i class=\"glyphicon glyphicon-calendar\"></i>\n                        </button>\n                    </span>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label>Waktu Berita <strong class=\"required\">*</strong></label>\n                <div class=\"input-group col-md-3\">\n                    <div class=\"input-group-addon\">\n                        <i class=\"fa fa-clock-o\"></i>\n                    </div>\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"news.news_time\" name=\"news_time\" [textMask]=\"{mask: mask}\" required>\n                </div>\n            </div>\n            <!--<div class=\"form-group\">\n                <label>Munculkan Data Penulis & Editor ?</label>\n                <div class=\"pull-right\">\n                    <ui-switch [(ngModel)]=\"news.show_writer_data\" name=\"show_writer_data\" ></ui-switch>\n                </div>\n            </div>\n            div class=\"form-group\">\n                <label>Kontributor Berita</label>\n                <ng-select\n                    [options]=\"initDatas.contributor_ids\"\n                    name=\"contributor_id\"\n                    notFoundMsg=\"Tidak Ditemukan\"\n                    [(ngModel)]=\"news.contributor_id\">\n                </ng-select>\n            </div-->\n            <div class=\"form-group\">\n                <label>Kategori Berita <strong class=\"required\">*</strong></label>\n                <ng-select\n                    [options]=\"initDatas.newscategory_ids\"\n                    name=\"newscategory_id\"\n                    required\n                    [(ngModel)]=\"news.newscategory_id\">\n                </ng-select>\n            </div>\n            <div class=\"form-group\">\n                <label>Judul Berita <strong class=\"required\">*</strong></label>\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"news.title\" name=\"title\" required >\n            </div>\n            <div class=\"form-group\">\n                <label>Sub Judul Berita</label>\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"news.subheader\" name=\"subheader\"  >\n            </div>\n            <!--<div class=\"form-group\">\n                <label>Tag Berita</label>\n                <p class=\"help-block\">Untuk Kategori Berita Koran Tag Jangan Diisi</p>\n                <tag-input [(ngModel)]=\"news.tags\"\n                        name=\"tags\"\n                        [placeholder]=\"'Ketik disini'\"\n                       [secondaryPlaceholder]=\"'Cari Tag'\"\n                        [onlyFromAutocomplete]=\"true\">\n                    <tag-input-dropdown [autocompleteObservable]=\"requestTagItems\">\n                        <ng-template let-item=\"item\" let-index=\"index\">\n                            {{ item.display }}\n                        </ng-template>\n                    </tag-input-dropdown>\n                </tag-input>\n            </div>-->\n            <div class=\"form-group\">\n                <label>Isi Berita</label>\n                <CKEDITOR [id]=\"'news.content'\" [value]=\"news.content\" (onChange)=\"onChangeContent($event)\" [type]=\"'simple'\"></CKEDITOR>\n            </div>\n\n        </div>\n\n        <div class=\"col-md-8 col-md-offset-2\">\n            <h3 class=\"box-title\">Foto Berita</h3>\n            <p class=\"help-block\">Ukuran 715 px x 450 px,Gambar Tidak Boleh Melebihi 90 Kb.</p>\n            <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" multiple  />\n            <p class=\"help-block\">Dapat memilih file lebih dari satu</p>\n\n            <!-- jika sukses di upload hilang dari list dibawah dan muncul di urutan terbawa daftar gambar -->\n            <ul class=\"uploader-errors\" *ngIf=\"initDatas.uploader_errors.length\">\n                <li *ngFor=\"let uploader_error of initDatas.uploader_errors\">\n                    {{ uploader_error }}\n                </li>\n            </ul>\n            <table class=\"table\">\n                <thead>\n                <tr>\n                    <th width=\"50%\">Name</th>\n                    <th>Progress</th>\n                    <th>Status</th>\n                    <th>Actions</th>\n                </tr>\n                </thead>\n                <tbody>\n                <tr *ngFor=\"let item of uploader.queue\">\n                    <td><strong>{{ item?.file?.name }}</strong></td>\n                    <td >\n                        <div class=\"progress\" style=\"margin-bottom: 0;\">\n                            <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\n                        </div>\n                    </td>\n                    <td class=\"text-center\">\n                        <span *ngIf=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\n                        <span *ngIf=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\n                        <span *ngIf=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\n                    </td>\n                    <td nowrap>\n                        <button type=\"button\" class=\"btn btn-success btn-xs\"\n                                (click)=\"item.upload()\" [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">\n                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\n                        </button>\n                        <button type=\"button\" class=\"btn btn-warning btn-xs\"\n                                (click)=\"item.cancel()\" [disabled]=\"!item.isUploading\">\n                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\n                        </button>\n                        <button type=\"button\" class=\"btn btn-danger btn-xs\"\n                                (click)=\"item.remove()\" [disabled]=\"item.isUploading || item.isSuccess\" >\n                            <span class=\"glyphicon glyphicon-trash\"></span> Remove\n                        </button>\n                    </td>\n                </tr>\n                </tbody>\n            </table>\n\n            <!-- <div>\n                <button type=\"button\" class=\"btn btn-success btn-s\"\n                        (click)=\"uploader.uploadAll()\" [disabled]=\"!uploader.getNotUploadedItems().length\">\n                    <span class=\"glyphicon glyphicon-upload\"></span> Upload all\n                </button>\n                <button type=\"button\" class=\"btn btn-warning btn-s\"\n                        (click)=\"uploader.cancelAll()\" [disabled]=\"!uploader.isUploading\">\n                    <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel all\n                </button>\n                <button type=\"button\" class=\"btn btn-danger btn-s\"\n                        (click)=\"uploader.clearQueue()\" [disabled]=\"!uploader.queue.length\">\n                    <span class=\"glyphicon glyphicon-trash\"></span> Remove all\n                </button>\n            </div> -->\n\n            <h3 class=\"box-title\">Foto Berita Terupload</h3>\n            <ul class=\"list-images list-inline row\">\n                <li *ngFor=\"let image of news.images ; let i = index\">\n                    <div class=\"col-md-12\">\n                        <h3>Gambar Berita {{ i+1 }}</h3>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <img src=\"{{image.small_url}}\" class=\"img-responsive\" />\n                        <div class=\"clearfix\"></div>\n                        <button type=\"button\" class=\"btn btn-danger btn-s\"\n                                (click)=\"deleteNewsImage(image)\" >\n                            <i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i> Hapus Gambar\n                        </button>\n                    \n                        <div class=\"clearfix\"></div>\n                        <br/>\n\n                        <button type=\"button\" class=\"btn btn-s\"\n                                (click)=\"selectACover(image)\" [ngClass]=\"buttonImageClass(image.cover)\" >\n                            <i *ngIf=\"!image.cover\" class=\"fa fa-picture-o\" aria-hidden=\"true\"></i><i *ngIf=\"image.cover\" class=\"fa fa-check\" cover-hidden=\"true\"></i> Set Cover\n                        </button>\n                    </div>\n                    <div class=\"col-md-9\">\n                        <CKEDITOR [id]=\"'image.image_captions.'+image.file_name\" [value]=\"image.image_caption\" (onChange)=\"onChangeImageContent($event, image)\" [type]=\"'small'\"></CKEDITOR>\n                    </div>\n                </li>\n            </ul>\n            <div class=\"form-group hide\">\n                <label>Status Berita <strong class=\"required\">*</strong></label>\n                <select class=\"form-control\" name=\"status\" [(ngModel)]=\"news.status\" required>\n                    <option value=\"1\"  >Publis Berita</option>\n                    <option value=\"2\"  >Tunda Berita</option>\n                    <option value=\"3\"  >Tolak Berita</option>\n                </select>\n            </div>\n            <div class=\"form- hide\">\n                <label>Tipe Berita</label>\n                <input class=\"form-control\" type=\"text\" value=\"{{this.newsService.getNewsTypeText(news.news_type)}}\" disabled />\n            </div>\n            <button *ngIf=\"domService.savingData\" disabled type=\"button\" class=\"btn btn-lg btn-default\"><i class=\"fa fa-spinner fa-spin fa-fw\"></i> Menyimpan</button>\n            <button *ngIf=\"!domService.savingData\" [disabled]=\"domService.savingData\" (click)=\"createNews()\" type=\"button\" class=\"btn btn-lg btn-default\"><i class=\"fa fa-book\" aria-hidden=\"true\"></i> Simpan</button>\n        </div>\n\n    </form>\n</div>\n\n<div class=\"modal fade\" bsModal #writingRoleModal=\"bs-modal\" [config]=\"{keyboard: true, ignoreBackdropClick:false}\"\n     tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBigModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog \">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Static modal</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"writingRoleModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" [innerHTML]=\"initDatas.writingRole\"></div>\n      <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"writingRoleModal.hide()\">Tutup</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 348:
/***/ (function(module, exports) {

module.exports = "<ol class=\"breadcrumb\">\n\t<li><a routerLink=\"/\">Home</a></li>\n\t<li><a routerLink=\"/news\">Daftar Berita</a></li>\n\t<li class=\"active\">Ubah Berita</li>\n</ol>\n\n<div *ngIf=\"initDatas.errorLoad\" class=\"text-center center-block\">\n\t<p class=\"text-error\">Gagal Memuat Data..</p>\n\t<button (click)=\"getNews()\" type=\"button\" class=\"btn btn-default\">Refresh Ulang <i class=\"fa fa-refresh\" aria-hidden=\"true\"></i></button>\n</div>\n\n<div class=\"row\" *ngIf=\"news\">\n    <form method=\"POST\" class=\"operator-form\" action=\"\" accept-charset=\"UTF-8\" role=\"form\">\n        <div class=\"col-md-8 col-md-offset-2\">\n            <h3 class=\"box-title\">Data Isi Berita</h3>\n            <div *ngIf=\"initDatas.errorMessages.length\" class=\"alert alert-danger\">\n                <strong>Error!</strong>\n                <ul>\n                    <li *ngFor=\"let errorMessage of initDatas.errorMessages\">\n                        {{ errorMessage }}\n                    </li>\n                </ul>\n            </div>\n            <div *ngIf=\"initDatas.successMessage\" class=\"alert alert-success\">\n                <strong>Sukses!</strong> Berita berhasil diubah\n            </div>\n\n            <!--button id=\"aturan-penulisan\" type=\"button\" class=\"btn btn-primary\" (click)=\"writingRoleModal.show()\">Aturan Penulisan</button-->\n            <div class=\"form-group\">\n                <label>Tanggal Berita <strong class=\"required\">*</strong></label>\n                <div class=\"input-group col-md-3\">\n                    <input class=\"form-control\" placeholder=\"Dari Tanggal\" ngx-mydatepicker name=\"news_date\"\n                            [(ngModel)]=\"news.news_date\" [options]=\"myDatePickerOptions\" #dp1=\"ngx-mydatepicker\"/>\n                    <span class=\"input-group-btn\">\n                        <button type=\"button\" class=\"btn btn-default\" (click)=\"dp1.clearDate()\">\n                            <i class=\"glyphicon glyphicon-remove\"></i>\n                        </button>\n                        <button type=\"button\" class=\"btn btn-default\" (click)=\"dp1.toggleCalendar()\">\n                            <i class=\"glyphicon glyphicon-calendar\"></i>\n                        </button>\n                    </span>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label>Waktu Berita <strong class=\"required\">*</strong></label>\n                <div class=\"input-group col-md-3\">\n                    <div class=\"input-group-addon\">\n                        <i class=\"fa fa-clock-o\"></i>\n                    </div>\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"news.news_time\" name=\"news_time\" [textMask]=\"{mask: mask}\" required>\n                </div>\n            </div>\n            <!-- <div class=\"form-group\">\n                <label>Munculkan Data Penulis & Editor ?</label>\n                <div class=\"pull-right\">\n                    <ui-switch [(ngModel)]=\"news.show_writer_data\" name=\"show_writer_data\" ></ui-switch>\n                </div>\n            </div> -->\n            <!--div class=\"form-group\">\n                <label>Kontributor Berita</label>\n                <ng-select\n                    [options]=\"initDatas.contributor_ids\"\n                    name=\"contributor_id\"\n                    notFoundMsg=\"Tidak Ditemukan\"\n                    [(ngModel)]=\"news.contributor_id\">\n                </ng-select>\n            </div-->\n            <div class=\"form-group\">\n                <label>Kategori Berita <strong class=\"required\">*</strong></label>\n                <ng-select\n                    [options]=\"initDatas.newscategory_ids\"\n                    name=\"newscategory_id\"\n                    required\n                    [(ngModel)]=\"news.newscategory_id\">\n                </ng-select>\n            </div>\n            <div class=\"form-group\">\n                <label>Judul Berita <strong class=\"required\">*</strong></label>\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"news.title\" name=\"title\" required >\n            </div>\n            <div class=\"form-group\">\n                <label>Sub Judul Berita</label>\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"news.subheader\" name=\"subheader\"  >\n            </div>\n\n            <!-- <div class=\"form-group\">\n                <label>Tag Berita</label>\n                <p class=\"help-block\">Untuk Kategori Berita Koran Tag Jangan Diisi</p>\n                <tag-input [(ngModel)]=\"news.tags\"\n                        name=\"tags\"\n                        [placeholder]=\"'Ketik disini'\"\n                       [secondaryPlaceholder]=\"'Cari Tag'\"\n                        [onlyFromAutocomplete]=\"true\">\n                    <tag-input-dropdown [autocompleteObservable]=\"requestTagItems\">\n                        <ng-template let-item=\"item\" let-index=\"index\">\n                            {{ item.display }}\n                        </ng-template>\n                    </tag-input-dropdown>\n                </tag-input>\n            </div> -->\n            <div class=\"form-group\">\n                <label>Isi Berita</label>\n                <CKEDITOR [id]=\"'news.content'\" [value]=\"news.content\" (onChange)=\"onChangeContent($event)\" [type]=\"'simple'\"></CKEDITOR>\n            </div>\n        </div>\n\n        <div class=\"col-md-8 col-md-offset-2\">\n            <hr/>\n            <h3 class=\"box-title\">Foto Berita</h3>\n            <p class=\"help-block\">Ukuran 715 px x 450 px,Gambar Tidak Boleh Melebihi 90 Kb.</p>\n            <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" multiple  />\n            <p class=\"help-block\">Dapat memilih file lebih dari satu</p>\n\n            <!-- jika sukses di upload hilang dari list dibawah dan muncul di urutan terbawa daftar gambar -->\n            <ul class=\"uploader-errors\" *ngIf=\"initDatas.uploader_errors.length\">\n                <li *ngFor=\"let uploader_error of initDatas.uploader_errors\">\n                    {{ uploader_error }}\n                </li>\n            </ul>\n            <table class=\"table\">\n                <thead>\n                <tr>\n                    <th width=\"50%\">Name</th>\n                    <th>Progress</th>\n                    <th>Status</th>\n                    <th>Actions</th>\n                </tr>\n                </thead>\n                <tbody>\n                <tr *ngFor=\"let item of uploader.queue\">\n                    <td><strong>{{ item?.file?.name }}</strong></td>\n                    <td >\n                        <div class=\"progress\" style=\"margin-bottom: 0;\">\n                            <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\n                        </div>\n                    </td>\n                    <td class=\"text-center\">\n                        <span *ngIf=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\n                        <span *ngIf=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\n                        <span *ngIf=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\n                    </td>\n                    <td nowrap>\n                        <button type=\"button\" class=\"btn btn-success btn-xs\"\n                                (click)=\"item.upload()\" [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">\n                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\n                        </button>\n                        <button type=\"button\" class=\"btn btn-warning btn-xs\"\n                                (click)=\"item.cancel()\" [disabled]=\"!item.isUploading\">\n                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\n                        </button>\n                        <button type=\"button\" class=\"btn btn-danger btn-xs\"\n                                (click)=\"item.remove()\" [disabled]=\"item.isUploading || item.isSuccess\" >\n                            <span class=\"glyphicon glyphicon-trash\"></span> Remove\n                        </button>\n                    </td>\n                </tr>\n                </tbody>\n            </table>\n\n            <!-- <div>\n                <button type=\"button\" class=\"btn btn-success btn-s\"\n                        (click)=\"uploader.uploadAll()\" [disabled]=\"!uploader.getNotUploadedItems().length\">\n                    <span class=\"glyphicon glyphicon-upload\"></span> Upload all\n                </button>\n                <button type=\"button\" class=\"btn btn-warning btn-s\"\n                        (click)=\"uploader.cancelAll()\" [disabled]=\"!uploader.isUploading\">\n                    <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel all\n                </button>\n                <button type=\"button\" class=\"btn btn-danger btn-s\"\n                        (click)=\"uploader.clearQueue()\" [disabled]=\"!uploader.queue.length\">\n                    <span class=\"glyphicon glyphicon-trash\"></span> Remove all\n                </button>\n            </div> -->\n\n            <h3 class=\"box-title\">Foto Berita Terupload</h3>\n            <ul class=\"list-images list-inline row\">\n                <li *ngFor=\"let image of news.images ; let i = index\">\n                    <div class=\"col-md-12\">\n                        <h3>Gambar Berita {{ i+1 }}</h3>\n                        <p *ngIf=\"image.id === null\" class=\"help-block\"><strong>Gambar belum disimpan ke berita.</strong></p>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <img src=\"{{image.small_url}}\" class=\"img-responsive\" />\n                        <div class=\"clearfix\"></div>\n                        <button type=\"button\" class=\"btn btn-danger btn-s\"\n                                (click)=\"deleteNewsImage(image)\" >\n                            <i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i> Hapus Gambar\n                        </button>\n                    \n                        <div class=\"clearfix\"></div>\n                        <br/>\n\n                        <button type=\"button\" class=\"btn btn-s\"\n                                (click)=\"selectACover(image)\" [ngClass]=\"buttonImageClass(image.cover)\" >\n                            <i *ngIf=\"!image.cover\" class=\"fa fa-picture-o\" aria-hidden=\"true\"></i><i *ngIf=\"image.cover\" class=\"fa fa-check\" cover-hidden=\"true\"></i> Set Cover\n                        </button>\n                    </div>\n                    <div class=\"col-md-9\">\n                        <CKEDITOR [id]=\"'image.image_captions.'+image.file_name\" [value]=\"image.image_caption\" (onChange)=\"onChangeImageContent($event, image)\" [type]=\"'small'\"></CKEDITOR>\n                    </div>\n                </li>\n            </ul>\n            <div class=\"form-group hide\">\n                <label>Status Berita <strong class=\"required\">*</strong></label>\n                <select class=\"form-control\" name=\"status\" [(ngModel)]=\"news.status\" required>\n                    <option value=\"1\"  >Publis Berita</option>\n                    <option value=\"2\"  >Tunda Berita</option>\n                    <option value=\"3\"  >Tolak Berita</option>\n                </select>\n            </div>\n            <div class=\"form-group hide\">\n                <label>Tipe Berita</label>\n                <input class=\"form-control\" type=\"text\" value=\"{{this.newsService.getNewsTypeText(news.news_type)}}\" disabled />\n            </div>\n            <button *ngIf=\"domService.savingData\" disabled type=\"button\" class=\"btn btn-lg btn-default pull-right\"><i class=\"fa fa-spinner fa-spin fa-fw\"></i> Menyimpan</button>\n            <button *ngIf=\"!domService.savingData\" [disabled]=\"domService.savingData\" (click)=\"updateNews()\" type=\"button\" class=\"btn btn-lg btn-default pull-right\"><i class=\"fa fa-book\" aria-hidden=\"true\"></i> Simpan</button>\n        </div>\n\n    </form>\n</div>\n\n<div class=\"modal fade\" bsModal #writingRoleModal=\"bs-modal\" [config]=\"{keyboard: true, ignoreBackdropClick:false}\"\n     tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBigModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog \">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Static modal</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"writingRoleModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" [innerHTML]=\"initDatas.writingRole\"></div>\n      <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"writingRoleModal.hide()\">Tutup</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 349:
/***/ (function(module, exports) {

module.exports = "<ol class=\"breadcrumb\">\r\n\t<li><a routerLink=\"/\">Home</a></li>\r\n\t<li class=\"active\">Daftar Berita</li>\r\n</ol>\r\n\r\n<form class=\"form-inline table-index-search pull-right\">\r\n\t<label class=\"sr-only\" for=\"inlineFormInput\">Dari Tanggal</label>\r\n\t<div class=\"input-group\">\r\n\t\t<input class=\"form-control\" placeholder=\"Dari Tanggal\" ngx-mydatepicker name=\"searchForm.start_date\"\r\n\t\t\t\t[(ngModel)]=\"searchForm.start_date\" [options]=\"myDatePickerOptions\" #dp1=\"ngx-mydatepicker\"/>\r\n\t\t<span class=\"input-group-btn\">\r\n\t\t\t<button type=\"button\" class=\"btn btn-default\" (click)=\"dp1.clearDate()\">\r\n\t\t\t\t<i class=\"glyphicon glyphicon-remove\"></i>\r\n\t\t\t</button>\r\n\t\t\t<button type=\"button\" class=\"btn btn-default\" (click)=\"dp1.toggleCalendar()\">\r\n\t\t\t\t<i class=\"glyphicon glyphicon-calendar\"></i>\r\n\t\t\t</button>\r\n\t\t</span>\r\n\t</div>\r\n\r\n\t<label class=\"sr-only\" for=\"inlineFormInput\">Sampai Tanggal</label>\r\n\t<div class=\"input-group\">\r\n\t\t<input class=\"form-control\" placeholder=\"Sampai Tanggal\" ngx-mydatepicker name=\"searchForm.end_date\"\r\n\t\t\t\t[(ngModel)]=\"searchForm.end_date\" [options]=\"myDatePickerOptions\" #dp2=\"ngx-mydatepicker\"/>\r\n\t\t<span class=\"input-group-btn\">\r\n\t\t\t<button type=\"button\" class=\"btn btn-default\" (click)=\"dp2.clearDate()\">\r\n\t\t\t\t<i class=\"glyphicon glyphicon-remove\"></i>\r\n\t\t\t</button>\r\n\t\t\t<button type=\"button\" class=\"btn btn-default\" (click)=\"dp2.toggleCalendar()\">\r\n\t\t\t\t<i class=\"glyphicon glyphicon-calendar\"></i>\r\n\t\t\t</button>\r\n\t\t</span>\r\n\t</div>\r\n\r\n\t<label class=\"sr-only\" for=\"inlineFormInput\">Kategori</label>\r\n\t<div class=\"input-group ng-select\">\r\n\t\t<ng-select\r\n\t\t\t\t[options]=\"initDatas.newscategory_ids\"\r\n\t\t\t\tplaceholder=\"Kategori\"\r\n\t\t\t\tname=\"searchForm.newscategory_id\"\r\n\t\t\t\t[(ngModel)]=\"searchForm.newscategory_id\">\r\n\t\t</ng-select>\r\n\t</div>\r\n\t\r\n\t<label class=\"sr-only\" for=\"inlineFormInput\">Judul</label>\r\n  \t<input type=\"text\" class=\"form-control\" [(ngModel)]=\"searchForm.title\" name=\"searchForm.title\" placeholder=\"Judul\">\r\n\r\n\t<label class=\"sr-only\" for=\"inlineFormInput\">User</label>\r\n\t<div class=\"input-group ng-select\">\r\n\t\t<ng-select\r\n\t\t\t\t[options]=\"initDatas.writers\"\r\n\t\t\t\tplaceholder=\"User\"\r\n\t\t\t\tname=\"searchForm.writer\"\r\n\t\t\t\t[(ngModel)]=\"searchForm.created_by\">\r\n\t\t</ng-select>\r\n\t</div>\r\n\r\n\t<button type=\"button\" (click)=\"searchData()\" class=\"btn btn-primary\"><i class=\"fa fa-search\" aria-hidden=\"true\"></i> Cari</button>\r\n</form>\r\n<div class=\"clearfix\"></div>\r\n<div *ngIf=\"domService.loadingData\" id=\"loader-page\">\r\n\t<div class=\"loader-page-inner\">\r\n\t\t<img src=\"{{ configs.baseAsset }}assets/images/loader/watch.gif\" />\r\n\t\t\r\n\t\t<span>Menunggu..</span>\r\n\t</div>\r\n</div>\r\n\r\n<div *ngIf=\"initDatas.errorLoad\" class=\"text-center center-block\">\r\n\t<p class=\"text-error\">Gagal Memuat Data..</p>\r\n\t<button (click)=\"getNews()\" type=\"button\" class=\"btn btn-default\">Refresh Ulang <i class=\"fa fa-refresh\" aria-hidden=\"true\"></i></button>\r\n</div>\r\n\r\n<div *ngIf=\"!domService.loadingData && !initDatas.errorLoad && pageNews\">\r\n\t<div class=\"table-responsive\">\r\n\t\t<table class=\"table table-hover table-index-data\">\r\n\t\t\t<thead>\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<th class=\"sortable\" (click)=\"sortData('title')\">Judul <i *ngIf=\"table_sort_by_field === 'title' && table_sort_by_order === 'asc'\" class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i><i *ngIf=\"table_sort_by_field === 'title' && table_sort_by_order === 'desc'\" class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i></th>\r\n\t\t\t\t\t<th class=\"sortable\" (click)=\"sortData('posted_at')\">Tanggal <i *ngIf=\"table_sort_by_field === 'posted_at' && table_sort_by_order === 'asc'\" class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i><i *ngIf=\"table_sort_by_field === 'posted_at' && table_sort_by_order === 'desc'\" class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i></th>\r\n\t\t\t\t\t<th class=\"sortable\" (click)=\"sortData('newscategory_id')\">Kategori <i *ngIf=\"table_sort_by_field === 'newscategory_id' && table_sort_by_order === 'asc'\" class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i><i *ngIf=\"table_sort_by_field === 'newscategory_id' && table_sort_by_order === 'desc'\" class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i></th>\r\n\t\t\t\t\t<th>Gambar</th>\r\n\t\t\t\t\t<th class=\"sortable\" (click)=\"sortData('created_by')\">Penulis <i *ngIf=\"table_sort_by_field === 'created_by' && table_sort_by_order === 'asc'\" class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i><i *ngIf=\"table_sort_by_field === 'created_by' && table_sort_by_order === 'desc'\" class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i></th>\r\n\t\t\t\t\t<th class=\"sortable\" (click)=\"sortData('status')\">Status <i *ngIf=\"table_sort_by_field === 'status' && table_sort_by_order === 'asc'\" class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i><i *ngIf=\"table_sort_by_field === 'status' && table_sort_by_order === 'desc'\" class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i></th>\r\n\t\t\t\t\t<th></th>\r\n\t\t\t\t</tr>\r\n\t\t\t</thead>\r\n\t\t\t<tbody>\r\n\t\t\t\t<tr *ngFor=\"let data of pageNews.data ; let i = index\">\r\n\t\t\t\t\t<td>{{ data.title }}</td>\r\n\t\t\t\t\t<td>{{ data.posted_at }}</td>\r\n\t\t\t\t\t<td>{{ data.newscategory.name }}</td>\r\n\t\t\t\t\t<td *ngIf=\"data.image_cover !== null\"><img src=\"{{data.image_cover.small_url}}\" class=\"img-responsive img-data\"/></td>\r\n\t\t\t\t\t<td *ngIf=\"data.image_cover === null\">-</td>\r\n\t\t\t\t\t<td>{{ data.writer.first_name }} {{ data.writer.last_name }}</td>\r\n\t\t\t\t\t<td>{{ data.status_text }}</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t<div class=\"pull-right\">\r\n\t\t\t\t\t\t\t<a class=\"btn btn-primary btn-sm edit-button\" title=\"Lihat\" (click)=\"showDetailNews(data.id)\" ><i class=\"fa fa-search\" aria-hidden=\"true\"></i></a>&nbsp;\r\n\t\t\t\t\t\t\t<a class=\"btn btn-warning btn-sm show-button\" [routerLink]=\"[ data.id, 'edit']\" title=\"Ubah\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>&nbsp;\r\n\t\t\t\t\t\t\t<a class=\"btn btn-danger btn-sm show-button\" (click)=\"deleteNews(data.id, data.title)\" title=\"Hapus\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr>\r\n\t\t\t</tbody>\r\n\t\t</table>\r\n\t</div>\r\n\t<pagination [totalItems]=\"pageNews.total\" [itemsPerPage]=\"pageNews.per_page\" [(ngModel)]=\"currentPage\" [boundaryLinks]=\"true\" [rotate]=\"false\" [maxSize]=\"10\" (pageChanged)=\"pageChanged($event)\"></pagination>\r\n</div>\r\n\r\n\r\n<div id=\"newsModal\" class=\"modal fade\" bsModal #newsModal=\"bs-modal\" [config]=\"{keyboard: true, ignoreBackdropClick:false}\"\r\n     tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myBigModalLabel\" aria-hidden=\"true\" >\r\n  <div *ngIf=\"!showNews\" class=\"modal-dialog modal-lg\" >\r\n\t  <div class=\"modal-content\">\r\n\t  \t<div class=\"modal-body\"><p>Sedang Memuat...</p></div>\r\n\t  </div>\r\n  </div>\r\n  <div *ngIf=\"showNews\" class=\"modal-dialog modal-lg\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h4 class=\"modal-title pull-left\">Detail Berita</h4>\r\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"newsModal.hide()\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n\t\t  <h3>{{ showNews.title }}</h3>\r\n\t\t  <h4 *ngIf=\"showNews.subheader !== '' \">{{ showNews.subheader }}</h4>\r\n\t\t  <p class=\"news-date\">Di post pada : <strong>{{ showNews.posted_at }}</strong></p>\r\n\t\t  <p class=\"news-category\">Kategori : <strong>{{ showNews.newscategory.name }}</strong></p>\r\n\t\t  <p class=\"news-status\">Status : <strong>{{ showNews.status_text }}</strong></p>\r\n\t\t  <div *ngIf=\"showNews.images.length > 0\" class=\"news-images\">\r\n\t\t\t<ng-template [ngIf]=\"showNews.images.length === 1\">\r\n\t\t\t\t<img class=\"img-responsive\" src=\"{{ showNews.images[0].big_url }}\" /> \r\n\t\t\t\t<div *ngIf=\"showNews.images[0].image_caption !== ''\" class=\"single-caption\" [innerHTML]=\"showNews.images[0].image_caption\"></div>\r\n\t\t\t</ng-template>\r\n\t\t\t<carousel *ngIf=\"showNews.images.length > 1\" [interval]=\"false\">\r\n\t\t\t\t<slide *ngFor=\"let image of showNews.images; let index=index\">\r\n\t\t\t\t\t<img [src]=\"image.big_url\" [alt]=\"image.image_caption\">\r\n\t\t\t\t\t<div *ngIf=\"image.image_caption !== ''\" class=\"carousel-caption\" [innerHTML]=\"image.image_caption\"></div>\r\n\t\t\t\t</slide>\r\n\t\t\t</carousel>\r\n\t\t  </div>\r\n\t\t  <div class=\"news-content\" [innerHTML]=\"showNews.content\"></div>\r\n\t  </div>\r\n      <div class=\"modal-footer\">\r\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"newsModal.hide()\">Tutup</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ 350:
/***/ (function(module, exports) {

module.exports = "<div id=\"sidebar-wrapper\">\r\n\t\t<ul class=\"sidebar-nav\">\r\n\t\t\t<li class=\"sidebar-brand\">\r\n\t\t\t\t<a routerLink=\"/\">\r\n\t\t\t\t\tAn-Manage Beta\r\n\t\t\t\t</a>\r\n\t\t\t</li>\r\n\t\t\t<menu-accordion [currentUrl]=\"currentUrl\"></menu-accordion>\r\n\t\t</ul>\r\n\t</div>"

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configs__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewsService = (function () {
    function NewsService(http) {
        this.http = http;
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' });
        this.newsUrl = __WEBPACK_IMPORTED_MODULE_2__configs__["a" /* Configs */].apiUrl + 'news';
        this.newsImageUrl = __WEBPACK_IMPORTED_MODULE_2__configs__["a" /* Configs */].apiUrl + 'files/news';
    }
    NewsService.prototype.getTemporaryNewsImages = function (news_id) {
        if (news_id === void 0) { news_id = null; }
        return this.http.get(this.newsImageUrl + '?news_id=' + (news_id === null ? '' : news_id))
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    NewsService.prototype.deleteNewsImage = function (id, url, news_id) {
        if (url === void 0) { url = null; }
        if (news_id === void 0) { news_id = null; }
        return this.http.delete(this.newsImageUrl + '/' + (id === null ? url : id) + '?news_id=' + (news_id === null ? '' : news_id), { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    NewsService.prototype.index = function (arrParams) {
        if (arrParams === void 0) { arrParams = []; }
        var params = [];
        Object.keys(arrParams).map(function (k, i) {
            params.push(k + '=' + arrParams[k]);
        });
        return this.http.get(this.newsUrl + (params.length ? '?' + params.join('&') : ''))
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    NewsService.prototype.create = function (data) {
        return this.http
            .post(this.newsUrl, data, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json().data; })
            .catch(this.handleError);
    };
    NewsService.prototype.update = function (data) {
        return this.http
            .put(this.newsUrl + '/' + data.id, data, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json().data; })
            .catch(this.handleError);
    };
    NewsService.prototype.delete = function (id) {
        return this.http
            .delete(this.newsUrl + '/' + id, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json().data; })
            .catch(this.handleError);
    };
    NewsService.prototype.getNewsTypeText = function (newsType) {
        var NewsTypeText = ['', 'Foto Dan Gambar', 'Hanya Teks', 'Hanya Foto', 'Video Dan Teks', 'Video Dan Gambar', 'Video, Teks & Foto'];
        return NewsTypeText[newsType];
    };
    NewsService.prototype.getNewsType = function (content, newsImagesLength) {
        //1 = 'text + image', 2 = 'texttype', 3 = 'imagetype' , 4 = 'video + text' , 5 = 'video + image', 6 = 'video + text + image'
        var newsType = 2;
        var hasVideo = false;
        content = typeof content === undefined || content === null ? '' : content;
        var hasText = content.length > 100 ? true : false;
        var div = document.createElement('div');
        div.classList.add('hidden');
        div.innerHTML = content;
        var iframes = div.getElementsByTagName('iframe');
        var key = /youtube.com/;
        for (var i = 0; i != iframes.length; ++i) {
            var match = iframes[i].src.search(key);
            if (match != -1) {
                hasVideo = true;
                break;
            }
        }
        if (newsImagesLength) {
            if (hasVideo)
                newsType = hasText ? 6 : 5;
            else
                newsType = hasText ? 1 : 3;
        }
        return newsType;
    };
    NewsService.prototype.getNews = function (id) {
        var url = this.newsUrl + "/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    NewsService.prototype.handleError = function (error) {
        //console.error('An error occurred', error)
        //return Promise.reject(error.message || error)
        return Promise.reject({
            'status': error.status,
            'body': typeof error._body !== 'undefined' ? JSON.parse(error._body) : null,
        });
    };
    return NewsService;
}());
NewsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], NewsService);

var _a;
//# sourceMappingURL=news.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configs__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewscategoryService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewscategoryService = (function () {
    function NewscategoryService(http) {
        this.http = http;
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' });
        this.newscategoryUrl = __WEBPACK_IMPORTED_MODULE_2__configs__["a" /* Configs */].apiUrl + 'newscategories';
    }
    NewscategoryService.prototype.getNewscategories = function (useFor) {
        if (useFor === void 0) { useFor = 'news'; }
        return this.http.get(this.newscategoryUrl + '?use_for=' + useFor)
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    NewscategoryService.prototype.handleError = function (error) {
        return Promise.reject({
            'status': error.status,
            'body': typeof error._body !== 'undefined' ? JSON.parse(error._body) : null,
        });
    };
    return NewscategoryService;
}());
NewscategoryService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], NewscategoryService);

var _a;
//# sourceMappingURL=newscategory.js.map

/***/ }),

/***/ 609:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(215);


/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalStorageService; });
var LocalStorageService = (function () {
    function LocalStorageService() {
    }
    LocalStorageService.prototype.save = function (name, val) {
        window.localStorage.setItem(name, JSON.stringify(val));
    };
    LocalStorageService.prototype.get = function (name) {
        if (window.localStorage.getItem(name) !== null)
            return JSON.parse(window.localStorage.getItem(name));
        else
            return null;
    };
    LocalStorageService.prototype.delete = function (name) {
        window.localStorage.removeItem(name);
    };
    return LocalStorageService;
}());

//# sourceMappingURL=localStorage.js.map

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_of__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_share__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_share___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_share__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__configs__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_toPromise__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoleService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



 //proper way to import the 'of' operator




var RoleService = (function () {
    function RoleService(http) {
        this.http = http;
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' });
        this.roleUrl = __WEBPACK_IMPORTED_MODULE_6__configs__["a" /* Configs */].apiUrl + 'roles';
    }
    RoleService.prototype.getRoles = function () {
        var _this = this;
        if (this.userRoles) {
            // if `data` is available just return it as `Observable`
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].of(this.userRoles);
        }
        else if (this.observable) {
            // if `this.observable` is set then the request is in progress
            // return the `Observable` for the ongoing request
            return this.observable;
        }
        else {
            // example header (not necessary)
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            // create the request, store the `Observable` for subsequent subscribers
            this.observable = this.http.get(this.roleUrl, {
                headers: headers
            })
                .map(function (response) {
                // when the cached data is available we don't need the `Observable` reference anymore
                _this.observable = null;
                if (response.status == 400) {
                    return "FAILURE";
                }
                else if (response.status == 200) {
                    _this.userRoles = response.json().data;
                    return _this.userRoles;
                }
                // make it shared so more than one subscriber can get the result
            })
                .share();
            return this.observable;
        }
    };
    RoleService.prototype.hasRole = function (roles, roleName) {
        var roleNameSplit = roleName.split('.');
        var data;
        if (roleNameSplit.length === 3)
            data = typeof roles[roleNameSplit[0]][roleNameSplit[1]][roleNameSplit[2]] !== 'undefined' ? roles[roleNameSplit[0]][roleNameSplit[1]][roleNameSplit[2]] : [];
        else if (roleNameSplit.length === 2)
            data = typeof roles[roleNameSplit[0]][roleNameSplit[1]] !== 'undefined' ? roles[roleNameSplit[0]][roleNameSplit[1]] : [];
        else
            data = typeof roles[roleNameSplit[0]] !== 'undefined' ? roles[roleNameSplit[0]] : [];
        return data.length ? true : false;
    };
    return RoleService;
}());
RoleService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], RoleService);

var _a;
//# sourceMappingURL=role.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configs__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TagService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TagService = (function () {
    function TagService(http) {
        this.http = http;
        this.tagUrl = __WEBPACK_IMPORTED_MODULE_2__configs__["a" /* Configs */].apiUrl + 'tags'; // URL to web api
    }
    TagService.prototype.getTags = function (keyword) {
        return this.http.get(this.tagUrl + '?q=' + keyword).map(this.extractData).catch(this.handleError);
    };
    TagService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data.map(function (item) { return item.title; }) || {};
    };
    TagService.prototype.handleError = function (error) {
        // In a real world app, you might use a remote logging infrastructure
        var errMsg;
        if (error instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Response */]) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(errMsg);
    };
    return TagService;
}());
TagService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], TagService);

var _a;
//# sourceMappingURL=tag.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configs__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserService = (function () {
    function UserService(http) {
        this.http = http;
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' });
        this.userUrl = __WEBPACK_IMPORTED_MODULE_2__configs__["a" /* Configs */].apiUrl + 'users';
    }
    UserService.prototype.getUserList = function () {
        return this.http.get(this.userUrl + '?use_for=list')
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    UserService.prototype.logOut = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__configs__["a" /* Configs */].baseDomain + 'logout')
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    UserService.prototype.handleError = function (error) {
        return Promise.reject({
            'status': error.status,
            'body': typeof error._body !== 'undefined' ? JSON.parse(error._body) : null,
        });
    };
    return UserService;
}());
UserService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], UserService);

var _a;
//# sourceMappingURL=user.js.map

/***/ })

},[609]);
//# sourceMappingURL=main.bundle.js.map