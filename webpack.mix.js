const { mix } = require('laravel-mix');

const paths = {
	'public': './public/assets/dashboard/',
    'jquery': './vendor/bower_components/jquery/',
    'bootstrap': './vendor/bower_components/bootstrap-sass/assets/',
    'fontAwesome': './vendor/bower_components/components-font-awesome/',
    'flatUi': './vendor/bower_components/flat-ui/'
}

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
        //copy(paths.flatUi + 'dist/fonts/**', paths.public + 'fonts').
        .copy('img/**', paths.public + 'img')
    	.sass('app.scss', paths.public + 'css/app.css')
        //.copy(paths.bootstrap + 'fonts/bootstrap/**', paths.public + 'fonts')
        .scripts([
            paths.jquery + 'dist/jquery.js',
            paths.bootstrap + 'javascripts/bootstrap.js',
            //paths.flatUi + 'dist/js/flat-ui.min.js',
            'jquery-validate/jquery.validate.min.js',
            'jquery-validate/messages_id.js',
            //'jquery-validate/jquery-validate.bootstrap-tooltip.min.js',
            'app.js'
        ], paths.public + 'js/app.js')
        //.copy(paths.fontAwesome + 'fonts/**', paths.public + 'fonts')
        
        //.version(paths.public + 'css/app.css')
        //.version(paths.public + 'js/app.js');
        ;
   // mix.browserSync({
   //     proxy: 'localhost:8000'
    //});
